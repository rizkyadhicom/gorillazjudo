<?php namespace App\Models;

class UserActivityModel extends BaseModel
{
    protected $table = "user_activities";
    protected $primaryKey = 'id';

    protected $allowedFields = ['user_id', 'activity', 'description', 'datetime', 'url'];

    function __construct()
    {
        parent::__construct();
    }

}