<?php namespace App\Models;

use CodeIgniter\Model;

class BaseModel extends Model
{
    protected $table;
    protected $primaryKey;

    public function getData($id = false)
    {
        if($id === false){
            return $this->table($this->table)
                ->get()
                ->getResultArray();
        } else {
            return $this->table($this->table)
                ->where($this->primaryKey, $id)
                ->get()
                ->getRowArray();
        }
    }

    public function getDataByWhere($where = [], $offset = false, $limit = false)
    {

        $model = $this->table($this->table);
        if(!empty($where)){
            foreach ($where as $field => $value){
                if(is_array($value)){
                    $model->whereIn($field, $value);
                }else{
                    $model->where($field, $value);
                }
            }
        }
        if($offset) $model->offset($offset);
        if($limit) $model->limit($limit);

        return $model->get()->getResultArray();
    }
    public function countDataByWhere($where = [])
    {
        $model = $this->table($this->table);
        if(!empty($where)){
            foreach ($where as $field => $value){
                if(is_array($value)){
                    $model->whereIn($field, $value);
                }else{
                    $model->where($field, $value);
                }
            }
        }

        return $model->countAllResults();
    }

    public function saveData($data)
    {
        return $this->db->table($this->table)->save($data);
    }

    public function deleteData($where = [])
    {
        $model = $this->table($this->table);
        if(!empty($where)){
            foreach ($where as $field => $value){
                if(is_array($value)){
                    $model->whereIn($field, $value);
                }else{
                    $model->where($field, $value);
                }
            }
            return $model->delete();
        }

    }
}