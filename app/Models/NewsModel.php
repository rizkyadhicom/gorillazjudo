<?php namespace App\Models;

use \App\Models\BaseModel;

class NewsModel extends BaseModel
{
    protected $table = "news";
    protected $primaryKey = 'id';

    protected $allowedFields = ['title', 'content', 'type', 'publish_date', 'author', 'slug', 'cover_image'];


}