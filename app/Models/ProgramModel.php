<?php namespace App\Models;

class ProgramModel extends BaseModel
{
    protected $table = "program";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'thumbnail', 'description', 'user_id', 'gokyo'];

    function __construct()
    {
        parent::__construct();
    }

}