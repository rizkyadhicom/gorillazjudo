<?php namespace App\Models;

class ProgramTrainingModel extends BaseModel
{
    protected $table = "program_training";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'program_id', 'user_id', 'score', 'status', 'video_url', 'date'];

    function __construct()
    {
        parent::__construct();
    }

    public function getScore($userId, $dateStart, $dateEnd)
    {
        $model = $this->table($this->table);
        $model->where('date >=', $dateStart);
        $model->where('date <=', $dateEnd);
        $model->where('user_id', $userId);
        $model->selectSum('score');
        $result = $model->get()->getRowArray();

        if (empty($result)){
            return 0;
        }else{
            return $result['score'];
        }
    }
    public function getAllScore($userId)
    {
        $model = $this->table($this->table);
        $model->where('user_id', $userId);
        $model->where('status', 'COMPLETED');
        $model->orderBy('date', 'desc');
        $result = $model->get()->getResultArray();

        if (empty($result)){
            return [];
        }else{
            return $result;
        }
    }
}