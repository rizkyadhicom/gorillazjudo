<?php namespace App\Models;

use App\Model;

class UserModel extends BaseModel
{
    protected $table = "users";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'birthday', 'parent_name', 'phone', 'gender', 'address', 'school_name', 'email', 'password', 'role', 'profile_image', 'about'];

    public function getUser($id = false)
    {
        if($id === false){
            return $this->table('users')
                ->get()
                ->getResultArray();
        } else {
            return $this->table('users')
                ->where('id', $id)
                ->get()
                ->getRowArray();
        }
    }

    public function getUserByWhere($where = [], $offset = false, $limit = false)
    {

        $model = $this->table('users');
        if(!empty($where)){
            foreach ($where as $field => $value){
                if(is_array($value)){
                    $model->whereIn($field, $value);
                }else{
                    $model->where($field, $value);
                }
            }
        }
        if($offset) $model->offset($offset);
        if($limit) $model->limit($limit);

        return $model->get()->getResultArray();
    }
    public function countUserByWhere($where = [])
    {
        $model = $this->table('users');
        if(!empty($where)){
            foreach ($where as $field => $value){
                if(is_array($value)){
                    $model->whereIn($field, $value);
                }else{
                    $model->where($field, $value);
                }
            }
        }

        return $model->countAllResults();
    }

    public function getUserByEmail($email = false)
    {
        if($email){
            return $this->table('users')
                ->where('email', $email)
                ->limit(1)
                ->get()
                ->getRowArray();
        }
        return false;
    }

    public function register($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

    public function saveUser($data)
    {
        return $this->db->table($this->table)->save($data);
    }
}