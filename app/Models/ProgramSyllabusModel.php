<?php namespace App\Models;

class ProgramSyllabusModel extends BaseModel
{
    protected $table = "program_syllabus";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'program_id', 'file_url'];

    function __construct()
    {
        parent::__construct();
    }

}