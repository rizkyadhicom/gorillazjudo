<?php namespace App\Models;

class UserAchievementModel extends BaseModel
{
    protected $table = "user_achievements";
    protected $primaryKey = 'id';

    protected $allowedFields = ['user_id', 'name', 'description', 'date'];

    function __construct()
    {
        parent::__construct();
    }

}