<?php namespace App\Models;

class ProgramVideoModel extends BaseModel
{
    protected $table = "program_videos";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'program_id', 'video_url'];

    function __construct()
    {
        parent::__construct();
    }

}