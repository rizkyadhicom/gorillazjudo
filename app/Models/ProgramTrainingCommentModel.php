<?php namespace App\Models;

class ProgramTrainingCommentModel extends BaseModel
{
    protected $table = "program_training_comment";
    protected $primaryKey = 'id';

    protected $allowedFields = ['name', 'program_training_id', 'comment', 'user_id', 'date'];

    function __construct()
    {
        parent::__construct();
    }

}