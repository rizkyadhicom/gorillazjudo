<?php namespace App\Controllers;

class News extends BaseController
{
    public function index()
    {
        $news = $this->newsModel->getData();
        return view('news', ['news'=>$news]);
    }

    public function detail($id)
    {
        $news = $this->newsModel->getData($id);
        return view('news_detail', ['news'=>$news]);
    }

    //--------------------------------------------------------------------

}
