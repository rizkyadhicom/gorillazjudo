<?php namespace App\Controllers;

class Training extends BaseController
{
    public function program($id)
    {
        $program = $this->programModel->getData($id);
        $program['countVideos'] = $this->programVideoModel->countDataByWhere(['program_id'=>$id]);
        $training = $this->programTrainingModel->getDataByWhere(['program_id'=>$id, 'user_id'=>$this->session->get('user_id')]);

        $lastStatus = [
            "status" => "NONE",
            "submited_date" => "",
            "score" => ""
        ];

        if(!empty($training)){
            foreach ($training as &$train){
                $train['comments'] = $this->programTrainingCommentModel->getDataByWhere(['program_training_id'=>$train['id']]);
                if(!empty($train['comments'])){
                    foreach ($train['comments'] as &$comment){
                        $comment['user'] = $this->userModel->getData($comment['user_id']);
                    }
                }
            }
            $countTraining = count($training);
            $lastTraining = $training[$countTraining-1];
            $lastStatus['status'] = $lastTraining["status"];
            $lastStatus['submited_date'] = $lastTraining["date"];
            if ($lastStatus == 'COMPLETED'){
                $lastStatus['score'] = $lastTraining["score"];
            }
        }
        return view('training', ['program' => $program, 'training'=>$training, 'lastStatus'=>$lastStatus]);
    }
    public function review($programId, $userId, $trainId = null)
    {
        $program = $this->programModel->getData($programId);
        $program['countVideos'] = $this->programVideoModel->countDataByWhere(['program_id'=>$programId]);
        if(!empty($trainId)){
            $training = $this->programTrainingModel->getDataByWhere(['program_id'=>$programId, 'user_id'=>$userId, 'id'=>$trainId]);
        }else{
            $training = $this->programTrainingModel->getDataByWhere(['program_id'=>$programId, 'user_id'=>$userId]);
        }

        $student = $this->userModel->getData($userId);

        $lastStatus = [
            "status" => "NONE",
            "submited_date" => "",
            "score" => ""
        ];

        if(!empty($training)){
            foreach ($training as &$train){
                $train['comments'] = $this->programTrainingCommentModel->getDataByWhere(['program_training_id'=>$train['id']]);
                if(!empty($train['comments'])){
                    foreach ($train['comments'] as &$comment){
                        $comment['user'] = $this->userModel->getData($comment['user_id']);
                    }
                }
            }
            $countTraining = count($training);
            $lastTraining = $training[$countTraining-1];
            $lastStatus['status'] = $lastTraining["status"];
            $lastStatus['submited_date'] = $lastTraining["date"];
            if ($lastStatus == 'COMPLETED'){
                $lastStatus['score'] = $lastTraining["score"];
            }
        }
        return view('training_instructor_review', ['program' => $program, 'training'=>$training, 'lastStatus'=>$lastStatus, 'student'=>$student]);
    }

    public function postcomment($id){
        $programTraining = $this->programTrainingModel->getData($id);
        $userId = $this->session->get('user_id');
        $userDetail = $this->userModel->getData($userId);
        if(!empty($programTraining) && !empty($userDetail)){
            $dataFromUser = [
                "program_training_id" => $id,
                "comment" => $this->request->getPost('comment'),
                "user_id" => $userDetail['id'],
                "date" => date('Y-m-d H:i:s')
            ];

            if($this->programTrainingCommentModel->save($dataFromUser)){
                if ($userDetail['role'] == 'instructor'){
                    $activityToSave = [
                        "user_id" => $programTraining['user_id'],
                        "description" => "Intructor Memberikan Komentar Pada Hasil Latihan Anda",
                        "datetime" => date('Y-m-d H:i:s'),
                        "url" => base_url('/training/program/'.$programTraining['program_id'])
                    ];
                    $this->userActivityModel->save($activityToSave);
                    return redirect()->redirect('/training/review/'.$programTraining['program_id'].'/'.$programTraining['user_id']);
                }else{
                    $programDetail = $this->programModel->getData($programTraining['program_id']);
                    $activityToSave = [
                        "user_id" => $programDetail['user_id'],
                        "description" => "Seorang Murid ". $userDetail['name'] ." Memberikan Komentar Pada Hasil Latihan Nya",
                        "datetime" => date('Y-m-d H:i:s'),
                        "url" => base_url('/training/review/'.$programTraining['program_id'].'/'.$programTraining['user_id'])
                    ];
                    $this->userActivityModel->save($activityToSave);
                    return redirect()->redirect('/training/program/'.$programTraining['program_id']);
                }
            }

        }
        return redirect()->redirect('/program');

    }
    public function postscore($id){
        $programTraining = $this->programTrainingModel->getData($id);
        $userId = $this->session->get('user_id');
        $userDetail = $this->userModel->getData($userId);
        if(!empty($programTraining) && !empty($userDetail)){
            $dataFromUser = [
                "id" => $id,
                "score" => $this->request->getPost('score'),
                "status" => 'COMPLETED'
            ];

            if($this->programTrainingModel->save($dataFromUser)){
                if ($userDetail['role'] == 'instructor'){
                    $activityToSave = [
                        "user_id" => $programTraining['user_id'],
                        "description" => "Intructor Memberikan Score Pada Hasil Latihan Anda",
                        "datetime" => date('Y-m-d H:i:s'),
                        "url" => base_url('/training/program/'.$programTraining['program_id'])
                    ];
                    $this->userActivityModel->save($activityToSave);
                    return redirect()->redirect('/training/review/'.$programTraining['program_id'].'/'.$programTraining['user_id']);
                }else{
                    return redirect()->redirect('/training/program/'.$programTraining['program_id']);
                }
            }
        }
        return redirect()->redirect('/program');

    }

    public function submit($id)
    {
        $program = $this->programModel->getData($id);
        if (!empty($program)){
            $dataToSave = [
                "program_id" => $program['id'],
                "user_id" => $this->session->get('user_id'),
                "score" => 0,
                "status" => 'REVIEW',
                "date" => date('Y-m-d', strtotime('now'))
            ];

            if(!empty($this->request->getPost('training_date'))){
                $dataToSave['date'] = $this->request->getPost('training_date');
            }

            //dd($this->request->getFile('training_result'));
            //Get the file
            if (!empty($this->request->getFile('training_result')) && !empty($this->request->getFile('training_result')->getName())){
                $file = $this->request->getFile('training_result');
                if(!$file->isValid()){
                    //dd($file->getErrorString());
                }

                $file->move('./assets/uploads');
                $dataToSave['video_url'] = $file->getName();
            }

            $this->programTrainingModel->save($dataToSave);

            return redirect()->to('/training/program/'.$program['id']);
        }
        return redirect()->to('/program');
    }


    public function instructor_program($id){
        $program = $this->programModel->getData($id);
        $program['countVideos'] = $this->programVideoModel->countDataByWhere(['program_id'=>$id]);
        $training = $this->programTrainingModel->getDataByWhere(['program_id'=>$id]);
        foreach ($training as &$train){
            $train['user'] = $this->userModel->getUser($train['user_id']);
        }
        return view('training_instructor_program', ['program' => $program, 'training'=>$training]);
    }

	//--------------------------------------------------------------------

}
