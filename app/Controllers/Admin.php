<?php namespace App\Controllers;

use CodeIgniter\HTTP\Request;

class Admin extends BaseController
{
    public function index()
    {
        if($this->session->has('is_login') && $this->session->get('is_login') && $this->session->get('user_role') == 'admin'){
            return redirect()->to('/admin/dashboard');
        }
        return view('cms_login');
    }
    public function checkLogin(){
        if($this->session->has('is_login') && $this->session->get('is_login') && $this->session->get('user_role') == 'admin'){
            return true;
        }else{
            return redirect()->to('/admin')->send() & exit();
        }
    }
    public function dashboard()
    {
        $this->checkLogin();
        return view('cms_dashboard');
    }
    public function students()
    {
        $this->checkLogin();
        $totalPage = 1;
        $page = 1;
        $offset = 0;
        $limit = 10;

        if($this->request->getGet('page')){
            $page = $this->request->getGet('page');
            $offset = ($page * $limit) - $limit;
        };
        $dataFromDB = $this->userModel->getUserByWhere(['role'=>'student'], $offset, $limit);
        $countDataFromDB = $this->userModel->countUserByWhere(['role'=>'student']);
        $totalPage = ceil($countDataFromDB / $limit);
        return view('cms_student', ['dataFromDB'=>$dataFromDB,'totalPage' => $totalPage, 'page' => $page, 'offset'=> $offset, 'limit' => $limit]);
    }
    public function student($id = null)
    {
        $this->checkLogin();
        $dataFromDB = $this->userModel->getUser($id);
        return view('cms_student_detail', ['dataFromDB'=>$dataFromDB]);
    }
    public function savestudent(){
        $this->checkLogin();
        $dataFromUser = [
            'id' => $this->request->getPost('id'),
            'name' => $this->request->getPost('name'),
            'birthday' => $this->request->getPost('birthday'),
            'parent_name' => $this->request->getPost('parent_name'),
            'phone' => $this->request->getPost('phone'),
            'email' => $this->request->getPost('email'),
            'gender' => $this->request->getPost('gender'),
            'address' => $this->request->getPost('address'),
            'school_name' => $this->request->getPost('school_name'),
        ];
        if($this->userModel->save($dataFromUser)){
            return redirect()->to('/admin/students');
        }

    }

    public function removestudent($id){
        $this->checkLogin();

        $isDataExist = $this->userModel->getUser($id);
        if(!empty($isDataExist)){
            $this->userModel->deleteData(['id'=>$id]);
            $this->userAchievementModel->deleteData(['user_id'=>$id]);
            $this->programTrainingModel->deleteData(['user_id'=>$id]);
        }

        return redirect()->to('/admin/students');

    }
    public function instructors()
    {
        $this->checkLogin();
        $totalPage = 1;
        $page = 1;
        $offset = 0;
        $limit = 10;

        if($this->request->getGet('page')){
            $page = $this->request->getGet('page');
            $offset = ($page * $limit) - $limit;
        };
        $dataFromDB = $this->userModel->getUserByWhere(['role'=>'instructor'], $offset, $limit);
        $countDataFromDB = $this->userModel->countUserByWhere(['role'=>'instructor']);
        $totalPage = ceil($countDataFromDB / $limit);
        return view('cms_instructor', ['dataFromDB'=>$dataFromDB,'totalPage' => $totalPage, 'page' => $page, 'offset'=> $offset, 'limit' => $limit]);
    }
    public function instructor($id = null)
    {
        $this->checkLogin();
        $dataFromDB = $this->userModel->getUser($id);
        return view('cms_instructor_detail', ['dataFromDB'=>$dataFromDB]);
    }
    public function saveinstructor(){
        $this->checkLogin();
        $dataFromUser = [
            'name' => $this->request->getPost('name'),
            'birthday' => $this->request->getPost('birthday'),
            'parent_name' => '',
            'phone' => $this->request->getPost('phone'),
            'email' => $this->request->getPost('email'),
            'gender' => $this->request->getPost('gender'),
            'address' => $this->request->getPost('address'),
            'school_name' => '',
            'role' => $this->request->getPost('role'),
        ];
        if (!empty($this->request->getPost('id'))){
            $dataFromUser['id'] = $this->request->getPost('id');
        }
        if (!empty($this->request->getPost('password'))){
            $dataFromUser['password'] = password_hash($this->request->getPost('password'), PASSWORD_DEFAULT);
        }
        if($this->userModel->save($dataFromUser)){
            return redirect()->to('/admin/instructors');
        }

    }

    public function removeinstructor($id){
        $this->checkLogin();

        $isDataExist = $this->userModel->getUser($id);
        if(!empty($isDataExist)){
            $this->userModel->deleteData(['id'=>$id]);
            $this->programModel->deleteData(['user_id'=>$id]);
        }

        return redirect()->to('/admin/instructors');

    }
    public function programs()
    {
        $this->checkLogin();
        $totalPage = 1;
        $page = 1;
        $offset = 0;
        $limit = 10;

        if($this->request->getGet('page')){
            $page = $this->request->getGet('page');
            $offset = ($page * $limit) - $limit;
        };
        $dataFromDB = $this->programModel->getDataByWhere([], $offset, $limit);
        $countDataFromDB = $this->programModel->countDataByWhere([]);
        $totalPage = $countDataFromDB > 0 ? ceil($countDataFromDB / $limit) : 1;
        return view('cms_program', ['dataFromDB'=>$dataFromDB,'totalPage' => $totalPage, 'page' => $page, 'offset'=> $offset, 'limit' => $limit]);
    }
    public function program($id = null)
    {
        $this->checkLogin();
        $instructorList = $this->userModel->getDataByWhere(['role'=>'instructor']);
        $dataFromDB = $this->programModel->getData($id);
        $dataFromDB['videos'] = $this->programVideoModel->getDataByWhere(['program_id'=>$id]);
        $dataFromDB['syllabus'] = $this->programSyllabusModel->getDataByWhere(['program_id'=>$id]);
        $goKyoList = ['Basic','Go Kyo 1','Go Kyo 2','Go Kyo 3','Go Kyo 4','Go Kyo 5'];
        return view('cms_program_detail', ['dataFromDB'=>$dataFromDB, 'instructorList'=>$instructorList, 'goKyoList'=>$goKyoList]);
    }

    public function removeprogram($id)
    {
        $data = $this->programModel->getData($id);
        if(!empty($data)){
            $ra = $this->programModel->delete($id);
        }
        return redirect()->to('/admin/programs');
    }
    public function saveprogram(){
        helper(['form']);
        $this->checkLogin();

        $dataFromUser = [
            'user_id' => $this->request->getPost('user_id'),
            'gokyo' => $this->request->getPost('gokyo'),
            'name' => $this->request->getPost('name'),
            'description' => $this->request->getPost('description')
        ];

        //Get the file
        if (!empty($this->request->getFile('thumbnail')) && !empty($this->request->getFile('thumbnail')->getName())){
            $file = $this->request->getFile('thumbnail');
            if(! $file->isValid())
                return $this->fail($file->getErrorString());

            $file->move('./assets/uploads');
            $dataFromUser['thumbnail'] = $file->getName();
        }

        if (!empty($this->request->getPost('id'))){
            $dataFromUser['id'] = $this->request->getPost('id');
        }
        if($this->programModel->save($dataFromUser)){
            if (!empty($this->request->getPost('id'))){
                $programId = $this->request->getPost('id');
            }else{
                $programId = $this->programModel->insertID();
            }
            $listVideoFile = $this->request->getFiles();
            if ($this->request->getPost('new_video_name')){
                $listNewVideoName = $this->request->getPost('new_video_name');
                $listNewVideoFile = $listVideoFile['new_video_file'];
                for ($i=0; $i<count($listNewVideoName); $i++){

                    $dataVideoFromUser = [
                        "name" => $listNewVideoName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listNewVideoFile[$i]->getName())){
                        $file = $listNewVideoFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataVideoFromUser['video_url'] = $file->getName();
                    }
                    $this->programVideoModel->save($dataVideoFromUser);

                }
            }
            if (!empty($listVideoFile['edit_video_file'])){
                $listEditVideoId = $this->request->getPost('edit_video_id');
                $listEditVideoName = $this->request->getPost('edit_video_name');
                $listEditVideoFile = $listVideoFile['edit_video_file'];
                for ($i=0; $i<count($listEditVideoName); $i++){

                    $dataVideoFromUser = [
                        "id" => $listEditVideoId[$i],
                        "name" => $listEditVideoName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listEditVideoFile[$i]->getName())){
                        $file = $listEditVideoFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataVideoFromUser['video_url'] = $file->getName();
                    }
                    $this->programVideoModel->save($dataVideoFromUser);

                }
            }

            if (!empty($this->request->getPost('deleted_video'))){
                $deletedIds = explode(',', $this->request->getPost('deleted_video'));
                foreach ($deletedIds as $deletedId){
                    $this->programVideoModel->delete($deletedId);
                }
            }
            if (!empty($this->request->getPost('deleted_syllabus'))){
                $deletedIds = explode(',', $this->request->getPost('deleted_syllabus'));
                foreach ($deletedIds as $deletedId){
                    $this->programSyllabusModel->delete($deletedId);
                }
            }

            $listSyllabusFile = $this->request->getFiles();
            if ($this->request->getPost('new_syllabus_name')){
                $listNewSyllabusName = $this->request->getPost('new_syllabus_name');
                $listNewSyllabusFile = $listSyllabusFile['new_syllabus_file'];
                for ($i=0; $i<count($listNewSyllabusName); $i++){

                    $dataSyllabusFromUser = [
                        "name" => $listNewSyllabusName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listNewSyllabusFile[$i]->getName())){
                        $file = $listNewSyllabusFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataSyllabusFromUser['file_url'] = $file->getName();
                    }
                    $this->programSyllabusModel->save($dataSyllabusFromUser);

                }
            }
            if (!empty($listSyllabusFile['edit_syllabus_file'])){
                $listEditSyllabusId = $this->request->getPost('edit_syllabus_id');
                $listEditSyllabusName = $this->request->getPost('edit_syllabus_name');
                $listEditSyllabusFile = $listSyllabusFile['edit_syllabus_file'];
                for ($i=0; $i<count($listEditSyllabusName); $i++){

                    $dataSyllabusFromUser = [
                        "id" => $listEditSyllabusId[$i],
                        "name" => $listEditSyllabusName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listEditSyllabusFile[$i]->getName())){
                        $file = $listEditSyllabusFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataSyllabusFromUser['file_url'] = $file->getName();
                    }
                    $this->programSyllabusModel->save($dataSyllabusFromUser);

                }
            }

            return redirect()->to('/admin/programs');
        }


    }


    public function newslist()
    {
        $this->checkLogin();
        $totalPage = 1;
        $page = 1;
        $offset = 0;
        $limit = 10;

        if($this->request->getGet('page')){
            $page = $this->request->getGet('page');
            $offset = ($page * $limit) - $limit;
        };
        $dataFromDB = $this->newsModel->getDataByWhere([], $offset, $limit);
        $countDataFromDB = $this->newsModel->countDataByWhere([]);
        $totalPage = $countDataFromDB > 0 ? ceil($countDataFromDB / $limit) : 1;
        return view('cms_news', ['dataFromDB'=>$dataFromDB,'totalPage' => $totalPage, 'page' => $page, 'offset'=> $offset, 'limit' => $limit]);
    }
    public function news($id = null)
    {
        $this->checkLogin();
        $dataFromDB = $this->newsModel->getData($id);
        return view('cms_news_detail', ['dataFromDB'=>$dataFromDB]);
    }
    public function savenews(){
        $this->checkLogin();

        $dataFromUser = [
            'title' => $this->request->getPost('title'),
            'content' => $this->request->getPost('content'),
            'type' => $this->request->getPost('type'),
            'publish_date' => $this->request->getPost('publish_date'),
            'author' => $this->request->getPost('author'),
            'slug' => ''
        ];

        //Get the file
        if (!empty($this->request->getFile('thumbnail')->getName())){
            $file = $this->request->getFile('thumbnail');
            if(! $file->isValid())
                return $this->fail($file->getErrorString());

            $file->move('./assets/uploads');
            $dataFromUser['cover_image'] = $file->getName();
        }

        if (!empty($this->request->getPost('id'))){
            $dataFromUser['id'] = $this->request->getPost('id');
        }
        if($this->newsModel->save($dataFromUser)){

            return redirect()->to('/admin/newslist');
        }


    }

    public function removenews($id){
        $this->checkLogin();

        $isDataExist = $this->newsModel->getData($id);
        if(!empty($isDataExist)){
            $this->newsModel->deleteData(['id'=>$id]);
        }

        return redirect()->to('/admin/newslist');

    }
	public function dovalidate(){
        $email = $this->request->getPost('email');
        $pass = $this->request->getPost('password');
        $cek_login = $this->userModel->getUserByEmail($email);

        if($cek_login == FALSE)
        {
            $this->session->setFlashdata('error_login', 'Email yang Anda masukan tidak terdaftar.');
            return redirect()->to('/admin');

        } else {

            if(password_verify($pass, $cek_login['password'])){

                if($cek_login['role'] == 'admin'){
                    $this->session->set('is_login', true);
                    $this->session->set('user_email', $cek_login['email']);
                    $this->session->set('user_name', $cek_login['name']);
                    $this->session->set('user_id', $cek_login['id']);
                    $this->session->set('user_role', $cek_login['role']);
                    return redirect()->to('/admin/dashboard');
                }else{

                    $this->session->setFlashdata('error_login', 'Email atau password yang Anda masukan salah.');
                    return redirect()->to('/admin');
                }
            } else {
                $this->session->setFlashdata('error_login', 'Email atau password yang Anda masukan salah.');
                return redirect()->to('/admin');
            }
        }
    }

}
