<?php namespace App\Controllers;

class Program extends BaseController
{
    public function index()
    {
        $where = [];
        $search = "";
        if (!empty($this->request->getGet('search'))){
            $where['name'] = $this->request->getGet('search');
            $search = $this->request->getGet('search');
        }
        $programAll = $this->programModel->getDataByWhere($where);
        $programList = [];
        foreach ($programAll as $program){
            if (!isset($programList[$program['gokyo']])){
                $programList[$program['gokyo']] = [];
            }
            $programList[$program['gokyo']][]= $program;
        }

        return view('program', ['programList' => $programList, 'search'=>$search]);
    }
    public function detail($id)
    {
        $program = $this->programModel->getData($id);
        $program['videos'] = $this->programVideoModel->getDataByWhere(['program_id'=>$id]);
        $program['syllabus'] = $this->programSyllabusModel->getDataByWhere(['program_id'=>$id]);
        $program['instructor'] = $this->userModel->getUser($program['user_id']);
        if($this->request->getGet('video_id')){
            $currentVideo = $this->programVideoModel->getData($this->request->getGet('video_id'));
        }else{
            $currentVideo = $program['videos'][0];
        }
        return view('program_detail', ['program' => $program, 'currentVideo'=>$currentVideo]);
    }

	//--------------------------------------------------------------------

}
