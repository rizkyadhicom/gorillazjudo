<?php namespace App\Controllers;

use App\Models\UserModel;
use CodeIgniter\Session\Session;

class Register extends BaseController
{

	public function index()
	{
		return view('register');
	}

	public function dovalidate(){
        $this->validation->setRules([
            'name' => 'required',
            'birthday' => 'required',
            'parent_name' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'school_name' => 'required',
            'email' => 'required|valid_email',
            'password' => 'required|trim|min_length[6]'
        ]);

        if ($this->validation->withRequest($this->request)->run() == FALSE) {
            $errors = $this->validation->getErrors();
            return redirect()->back()->with(['errors' => $errors, 'input' => $this->request->getPost()]);
        } else {
            $name = $this->request->getPost('name');
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            $pass = password_hash($password, PASSWORD_DEFAULT);
            $data = [
                'name' => $name,
                'birthday' => $this->request->getPost('birthday'),
                'parent_name' => $this->request->getPost('parent_name'),
                'phone' => $this->request->getPost('phone'),
                'gender' => $this->request->getPost('gender'),
                'address' => $this->request->getPost('address'),
                'school_name' => $this->request->getPost('school_name'),
                'email' => $email,
                'password' => $pass,
                'role' => 'student'
            ];

            $insert = $this->userModel->register($data);
            if($insert){
                echo '<script>alert("Sukses! Anda berhasil melakukan register. Silahkan login untuk mengakses data.");window.location.href="'.base_url('/login').'";</script>';
            }
        }
    }

	//--------------------------------------------------------------------

}
