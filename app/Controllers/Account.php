<?php namespace App\Controllers;

class Account extends BaseController
{

    function getLastDays($strDate){
        $listDate=[];
        $date=date_create(date("Y-m-d", strtotime($strDate)));
        for($i=0;$i<3;$i++){
            //$date = date_format($date,"Y-m-d");
            array_push($listDate, date_format($date,"Y-m-d"));
            date_modify($date,"-1 day");
        }
        return $listDate;
    }
    public function index($userId = null)
    {

        if(!empty($this->session->get('user_id')) && $userId == null){
            $userId = $this->session->get('user_id');
        }

        $user = $this->userModel->getUser($userId);
        if($user['role'] == 'student'){

            $listScore = $this->programTrainingModel->getAllScore($userId);
            $dayCount = 1;
            $weekCount = -1;
            $scoreCount = 0;
            $startDate = date_create(date("Y-m-d", strtotime('today')));
            $startDateStr = date_format($startDate,"Y-m-d");
            $weekDates = [];

            $listDates = [];
            $listValues = [];
            if(!empty($listScore)){
                foreach ($listScore as $i => $score){
                    $scoreValue = $score['score'];
                    if(in_array($score['date'], $weekDates) && $i != 0){
                        if (!isset($listValues[$weekCount])) $listValues[$weekCount] = 0;
                        $listValues[$weekCount] += $scoreValue;
                        $scoreCount += $scoreValue;
                    }else{
                        $weekCount += 1;
                        $startDate = date_create(date("Y-m-d", strtotime( $score['date'])));
                        $startDateStr = date_format($startDate,"Y-m-d");
                        $weekDates = $this->getLastDays($startDateStr);
                        $dEnd = date_format($startDate,"Y-m-d");
                        date_modify($startDate,"-3 days");
                        $dStart = date_format($startDate,"d/m/Y");
                        $listDates[$weekCount] = $dStart . "-" . $dEnd;
                        if (!isset($listValues[$weekCount])) $listValues[$weekCount] = 0;
                        $listValues[$weekCount] += $scoreValue;
                        $scoreCount += $scoreValue;
                    }
                    if ($weekCount == 6) break;
                }
            }
            if (count($listDates) < 6 || count($listValues) < 6){
                for($i=count($listDates);$i<6;$i++){
                    $listDates[$i] = "";
                }
                for($i=count($listValues);$i<6;$i++){
                    $listValues[$i] = "";
                }
            }

            $listDates = array_reverse($listDates);
            $listValues = array_reverse($listValues);
            $listChart = [];
            for($i=0;$i<6;$i++){
                $dataToChart = [
                    "x"=>$listDates[$i],
                    "y"=>$listValues[$i]
                ];
                array_push($listChart, $dataToChart);
            }
            $dataChart = ["data"=>$listChart, "label"=>$listDates, "total"=>$scoreCount];
            $achievements = $this->userAchievementModel->getDataByWhere(['user_id' => $userId]);
            return view('account', ['dataFromDB' => $user, 'achievements' => $achievements, 'dataChart'=>$dataChart, 'activities'=>[]]);
        }elseif ($user['role'] == 'instructor'){
            $students = $this->userModel->getDataByWhere(['role'=>'student']);
            $programs = $this->programModel->getDataByWhere(['user_id'=>$userId]);
            return view('account_instructor', ['dataFromDB' => $user, 'students'=>$students, 'programs'=>$programs]);
        }elseif ($user['role'] == 'admin'){
            return redirect()->to('/admin');
        }
    }

    public function profile($userId=null)
    {
        if(!$userId){
            $userId = $this->session->get('user_id');
        }
        $user = $this->userModel->getUser($userId);
        if($user['role'] == 'student'){
            $achievements = $this->userAchievementModel->getDataByWhere(['user_id' => $userId]);
            return view('account', ['dataFromDB' => $user, 'achievements' => $achievements]);
        }elseif ($user['role'] == 'instructor'){
            $students = $this->userModel->getDataByWhere(['role'=>'student']);
            $programs = $this->programModel->getDataByWhere(['user_id'=>$userId]);
            return view('account_instructor', ['dataFromDB' => $user, 'students'=>$students, 'programs'=>$programs]);
        }
    }
    public function edit()
    {
        $userId = $this->session->get('user_id');
        $user = $this->userModel->getUser($userId);
        return view('account_edit', ['dataFromDB' => $user]);
    }

    public function dovalidate(){
        $dataFromUser = [
            'id' => $this->request->getPost('id'),
            'name' => $this->request->getPost('name'),
            'birthday' => $this->request->getPost('birthday'),
            'parent_name' => $this->request->getPost('parent_name'),
            'phone' => $this->request->getPost('phone'),
            'email' => $this->request->getPost('email'),
            'gender' => $this->request->getPost('gender'),
            'address' => $this->request->getPost('address'),
            'school_name' => $this->request->getPost('school_name')
        ];

        //Get the file
        if (!empty($this->request->getFile('profile_image')->getName())){
            $file = $this->request->getFile('profile_image');
            if(! $file->isValid())
                return $this->fail($file->getErrorString());

            $file->move('./assets/uploads');
            $dataFromUser['profile_image'] = $file->getName();
        }
        if (!empty($this->request->getPost('password'))){
            if($this->request->getPost('password') == $this->request->getPost('repassword')){

            }else{

            }
        }

        if($this->userModel->save($dataFromUser)){

            return redirect()->to('/account');
        }
    }
    public function addachievement($userId)
    {
        $user = $this->userModel->getUser($userId);
        $user['achievements'] = $this->userAchievementModel->getDataByWhere(['user_id'=>$userId]);
        return view('account_achievements_edit', ['dataFromDB' => $user]);
    }
    public function doachievement($userId)
    {
        if ($this->request->getPost('new_achievement_title')){
            $listNewAchievementTitle = $this->request->getPost('new_achievement_title');
            $listNewAchievementDescription = $this->request->getPost('new_achievement_description');
            $listNewAchievementDate = $this->request->getPost('new_achievement_date');
            for ($i=0;$i<count($listNewAchievementTitle);$i++){
                $dataToInsert = [
                    "user_id" => $userId,
                    "name" => $listNewAchievementTitle[$i],
                    "description" => $listNewAchievementDescription[$i],
                    "date" => $listNewAchievementDate[$i],
                ];
                $this->userAchievementModel->save($dataToInsert);
            }
        }
        if ($this->request->getPost('edit_achievement_id')){
            $listEditAchievementId = $this->request->getPost('edit_achievement_id');
            $listEditAchievementTitle = $this->request->getPost('edit_achievement_title');
            $listEditAchievementDescription = $this->request->getPost('edit_achievement_description');
            $listEditAchievementDate = $this->request->getPost('edit_achievement_date');
            for ($i=0;$i<count($listEditAchievementId);$i++){
                $dataToUpdate = [
                    "id" => $listEditAchievementId[$i],
                    "name" => $listEditAchievementTitle[$i],
                    "description" => $listEditAchievementDescription[$i],
                    "date" => $listEditAchievementDate[$i],
                ];
                $this->userAchievementModel->save($dataToUpdate);
            }
        }
        return redirect()->to('/account/addachievement/'. $userId);
    }

    public function removeachievement($userId, $achievementId)
    {
        $data = $this->userAchievementModel->getData($achievementId);
        if(!empty($data) && $userId == $data['user_id']){
            $ra = $this->userAchievementModel->delete($achievementId);
        }
        return redirect()->to('/account/addachievement/'. $userId);
    }

    public function detail($id)
    {
        $program = $this->programModel->getData($id);
        $program['videos'] = $this->programVideoModel->getDataByWhere(['program_id'=>$id]);
        if($this->request->getGet('video_id')){
            $currentVideo = $this->programVideoModel->getData($this->request->getGet('video_id'));
        }else{
            $currentVideo = $program['videos'][0];
        }
        return view('program_detail', ['program' => $program, 'currentVideo'=>$currentVideo]);
    }

	//--------------------------------------------------------------------

}
