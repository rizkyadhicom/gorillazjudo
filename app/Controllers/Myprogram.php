<?php namespace App\Controllers;


class Myprogram extends BaseController
{
    public function __construct() {
        parent::__construct();
        if ($this->session->get('user_role') != 'instructor'){
            return redirect()->to('/program');
        }
    }
    public function index(){
        $userId = $this->session->get('user_id');
        $dataFromDB = $this->programModel->getDataByWhere(['user_id'=>$userId]);
        return view('my_program', ['programs'=>$dataFromDB]);

    }
    public function add()
    {
        $goKyoList = ['Basic','Go Kyo 1','Go Kyo 2','Go Kyo 3','Go Kyo 4','Go Kyo 5'];
        return view('add_subprogram', ['goKyoList'=>$goKyoList]);
    }
    public function edit($id = null)
    {
        $dataFromDB = $this->programModel->getData($id);
        $dataFromDB['videos'] = $this->programVideoModel->getDataByWhere(['program_id'=>$id]);
        return view('edit_program', ['dataFromDB'=>$dataFromDB]);
    }
    public function editsub($id = null)
    {
        $dataFromDB = $this->programModel->getData($id);
        $dataFromDB['videos'] = $this->programVideoModel->getDataByWhere(['program_id'=>$id]);
        $dataFromDB['syllabus'] = $this->programSyllabusModel->getDataByWhere(['program_id'=>$id]);
        $goKyoList = ['Basic','Go Kyo 1','Go Kyo 2','Go Kyo 3','Go Kyo 4','Go Kyo 5'];
        return view('edit_subprogram', ['dataFromDB'=>$dataFromDB, 'goKyoList'=>$goKyoList]);
    }

    public function saveprogram(){
        helper(['form']);

        $dataFromUser = [
            'user_id' => $this->session->get('user_id'),
            'gokyo' => $this->request->getPost('gokyo'),
            'name' => $this->request->getPost('name'),
            'description' => $this->request->getPost('description')
        ];

        //Get the file
        if (!empty($this->request->getFile('thumbnail')) && !empty($this->request->getFile('thumbnail')->getName())){
            $file = $this->request->getFile('thumbnail');
            if(! $file->isValid())
                return $this->fail($file->getErrorString());

            $file->move('./assets/uploads');
            $dataFromUser['thumbnail'] = $file->getName();
        }

        if (!empty($this->request->getPost('id'))){
            $dataFromUser['id'] = $this->request->getPost('id');
        }
        if($this->programModel->save($dataFromUser)){
            if (!empty($this->request->getPost('id'))){
                $programId = $this->request->getPost('id');
            }else{
                $programId = $this->programModel->insertID();
            }
            $listVideoFile = $this->request->getFiles();
            if ($this->request->getPost('new_video_name')){
                $listNewVideoName = $this->request->getPost('new_video_name');
                $listNewVideoFile = $listVideoFile['new_video_file'];
                for ($i=0; $i<count($listNewVideoName); $i++){

                    $dataVideoFromUser = [
                        "name" => $listNewVideoName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listNewVideoFile[$i]->getName())){
                        $file = $listNewVideoFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataVideoFromUser['video_url'] = $file->getName();
                    }
                    $this->programVideoModel->save($dataVideoFromUser);

                }
            }
            if (!empty($listVideoFile['edit_video_file'])){
                $listEditVideoId = $this->request->getPost('edit_video_id');
                $listEditVideoName = $this->request->getPost('edit_video_name');
                $listEditVideoFile = $listVideoFile['edit_video_file'];
                for ($i=0; $i<count($listEditVideoName); $i++){

                    $dataVideoFromUser = [
                        "id" => $listEditVideoId[$i],
                        "name" => $listEditVideoName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listEditVideoFile[$i]->getName())){
                        $file = $listEditVideoFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataVideoFromUser['video_url'] = $file->getName();
                    }
                    $this->programVideoModel->save($dataVideoFromUser);

                }
            }

            if (!empty($this->request->getPost('deleted_video'))){
                $deletedIds = explode(',', $this->request->getPost('deleted_video'));
                foreach ($deletedIds as $deletedId){
                    $this->programVideoModel->delete($deletedId);
                }
            }
            if (!empty($this->request->getPost('deleted_syllabus'))){
                $deletedIds = explode(',', $this->request->getPost('deleted_syllabus'));
                foreach ($deletedIds as $deletedId){
                    $this->programSyllabusModel->delete($deletedId);
                }
            }

            $listSyllabusFile = $this->request->getFiles();
            if ($this->request->getPost('new_syllabus_name')){
                $listNewSyllabusName = $this->request->getPost('new_syllabus_name');
                $listNewSyllabusFile = $listSyllabusFile['new_syllabus_file'];
                for ($i=0; $i<count($listNewSyllabusName); $i++){

                    $dataSyllabusFromUser = [
                        "name" => $listNewSyllabusName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listNewSyllabusFile[$i]->getName())){
                        $file = $listNewSyllabusFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataSyllabusFromUser['file_url'] = $file->getName();
                    }
                    $this->programSyllabusModel->save($dataSyllabusFromUser);

                }
            }
            if (!empty($listSyllabusFile['edit_syllabus_file'])){
                $listEditSyllabusId = $this->request->getPost('edit_syllabus_id');
                $listEditSyllabusName = $this->request->getPost('edit_syllabus_name');
                $listEditSyllabusFile = $listSyllabusFile['edit_syllabus_file'];
                for ($i=0; $i<count($listEditSyllabusName); $i++){

                    $dataSyllabusFromUser = [
                        "id" => $listEditSyllabusId[$i],
                        "name" => $listEditSyllabusName[$i],
                        "program_id" => $programId
                    ];
                    if(!empty($listEditSyllabusFile[$i]->getName())){
                        $file = $listEditSyllabusFile[$i];
                        if(! $file->isValid())
                            return $this->fail($file->getErrorString());

                        $file->move('./assets/uploads');
                        $dataSyllabusFromUser['file_url'] = $file->getName();
                    }
                    $this->programSyllabusModel->save($dataSyllabusFromUser);

                }
            }

            return redirect()->to('/myprogram');
        }


    }

    public function removeprogram($id)
    {
        $data = $this->programModel->getData($id);
        if(!empty($data)){
            $ra = $this->programModel->delete($id);
        }
        return redirect()->to('/myprogram');
    }
    public function addsub($idProgram)
    {
        return view('add_subprogram');
    }

	//--------------------------------------------------------------------

}
