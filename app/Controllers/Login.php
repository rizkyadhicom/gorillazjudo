<?php namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
		return view('login');
	}

	public function dovalidate(){
        $email = $this->request->getPost('email');
        $pass = $this->request->getPost('password');
        $cek_login = $this->userModel->getUserByEmail($email);

        if($cek_login == FALSE)
        {
            $this->session->setFlashdata('error_login', 'Email yang Anda masukan tidak terdaftar.');
            return redirect()->to('/login');

        } else {

            if(password_verify($pass, $cek_login['password'])){
                $this->session->set('is_login', true);
                $this->session->set('user_email', $cek_login['email']);
                $this->session->set('user_name', $cek_login['name']);
                $this->session->set('user_id', $cek_login['id']);
                $this->session->set('user_role', $cek_login['role']);

                return redirect()->to('/');

            } else {
                $this->session->setFlashdata('error_login', 'Email atau password yang Anda masukan salah.');
                return redirect()->to('/login');
            }
        }
    }

}
