<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use App\Models\NewsModel;
use App\Models\ProgramModel;
use App\Models\ProgramSyllabusModel;
use App\Models\ProgramTrainingCommentModel;
use App\Models\ProgramTrainingModel;
use App\Models\ProgramVideoModel;
use App\Models\UserAchievementModel;
use App\Models\UserActivityModel;
use App\Models\UserModel;
use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
	}


    public function __construct() {

        // Mendeklarasikan class ProductModel menggunakan $this->product
        $this->userModel = new UserModel();
        $this->userAchievementModel = new UserAchievementModel();
        $this->userActivityModel = new UserActivityModel();
        $this->programModel = new ProgramModel();
        $this->programVideoModel = new ProgramVideoModel();
        $this->programSyllabusModel = new ProgramSyllabusModel();
        $this->programTrainingModel = new ProgramTrainingModel();
        $this->programTrainingCommentModel = new ProgramTrainingCommentModel();
        $this->newsModel = new NewsModel();
        $this->session = \Config\Services::session();
        $this->validation =  \Config\Services::validation();
        /* Catatan:
        Apa yang ada di dalam function construct ini nantinya bisa digunakan
        pada function di dalam class Product
        */
    }

    public function saveActivity($description="", $url="", $userId=null){
	    $dataFromUser = [
            "description"=>$description,
            "url"=>$url
        ];
	    if($userId != null){
	        $dataFromUser['user_id'] = $userId;
        }else{
            if($this->session->has('is_login') && $this->session->has('user_id')){
                $dataFromUser['user_id'] = $this->session->get('user_id');
            }
        }
    }

    public function fail($errorMessage){
        log_message('error', $errorMessage);
//	    error_log($errorMessage);
    }

}
