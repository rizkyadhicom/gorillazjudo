<?= $this->extend('app') ?>

<?= $this->section('content') ?>

    <div class="page-section bg-alt border-bottom-2">
        <div class="container page__container">

            <div class="d-flex flex-column flex-lg-row align-items-center">
                <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                    <h1 class="h2 mb-8pt">Manage Program</h1>
                </div>
                <div class="ml-lg-16pt">
                    <a href="<?php echo base_url('myprogram/add'); ?>" class="btn btn-light">Add Program</a>
                </div>
            </div>

        </div>
    </div>


    <div class="page-section">
        <div class="container page__container">

            <div class="row">
                <div class="col-lg-12">

                    <div class="page-separator">
                        <div class="page-separator__text">Go Kyo</div>
                    </div>



                    <div class="row">
                        <?php foreach ($programs as $program) { ?>
                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary js-overlay mdk-reveal js-mdk-reveal " data-partial-height="44" data-toggle="popover" data-trigger="click">
                                <a href="<?php echo base_url('myprogram/editsub/'.$program['id']); ?>" class="js-image" data-position="">
                                    <img src="<?php echo base_url('/assets/uploads/'.$program['thumbnail']); ?>" alt="course" height="200px">
                                    <span class="overlay__content align-items-start justify-content-start">
                                        <span class="overlay__action card-body d-flex align-items-center">
                                            <i class="material-icons mr-4pt">edit</i>
                                            <span class="card-title text-white">Edit</span>
                                        </span>
                                    </span>
                                </a>
                                <div class="mdk-reveal__content">
                                    <div class="card-body">
                                        <div class="d-flex">
                                            <div class="flex">
                                                <a class="card-title mb-4pt" href="<?php echo base_url('myprogram/editsub/'.$program['id']); ?>"><?php echo $program['name'] ?></a>
                                            </div>
                                            <a href="<?php echo base_url('myprogram/editsub/'.$program['id']); ?>" class="ml-4pt material-icons text-black-20 card-course__icon-favorite">edit</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>

        </div>
    </div>


<?= $this->endSection() ?>
