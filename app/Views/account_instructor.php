<?= $this->extend('app') ?>



<?= $this->section('content') ?>
<div class="page-section bg-primary">
    <div class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-md-left">
        <img src="/assets/images/illustration/teacher/128/white.svg" width="104" class="mr-md-32pt mb-32pt mb-md-0" alt="instructor">
        <div class="flex mb-32pt mb-md-0">
            <h2 class="text-white mb-0"><?php echo $dataFromDB['name'] ?></h2>
            <p class="lead text-white-50 d-flex align-items-center">Instructor </p>
        </div>
    </div>
</div>

<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">
        <div class="row">
            <div class="col-md-6">
                <h4>About me</h4>
                <p><?php echo $dataFromDB['about'] ?></p>
            </div>
        </div>
    </div>
</div>

<div class="container page__container page-section">
    <div class="page-headline text-center">
        <h3>Programs</h3>
    </div>
    <div class="row card-group-row mb-48pt">
        <?php foreach ($programs as $program) { ?>
            <div class="col-sm-6 card-group-row__col">
                <div class="card card-sm card-group-row__card">
                    <div class="card-body d-flex align-items-center">
                        <a href="fixed-course.html" class="avatar avatar-4by3 overlay overlay--primary mr-12pt">
                            <img src="/assets/uploads/<?php echo $program['thumbnail'] ?>" alt="Angular Routing In-Depth" class="avatar-img rounded">
                            <span class="overlay__content"></span>
                        </a>
                        <div class="flex">
                            <a class="card-title mb-4pt" href="fixed-course.html"><?php echo $program['name'] ?></a>
                            <div class="d-flex align-items-center">
                                <small class="text-muted"><a href="<?php echo base_url('/training/instructor_program/'.$program['id']) ?>">See Training Result</a> </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="page-headline text-center">
        <h3>Students</h3>
    </div>
    <div class="row card-group-row mb-8pt">
        <?php foreach ($students as $student) { ?>
        <div class="col-sm-6 card-group-row__col">
            <div class="card card-sm card-group-row__card">
                <div class="card-body d-flex align-items-center">
                    <a href="fixed-course.html" class="avatar avatar-4by3 overlay overlay--primary mr-12pt">
                        <img src="/assets/images/paths/angular_routing_200x168.png" alt="Angular Routing In-Depth" class="avatar-img rounded">
                        <span class="overlay__content"></span>
                    </a>
                    <div class="flex">
                        <a class="card-title mb-4pt" href="fixed-course.html"><?php echo $student['name'] ?></a>
                        <div class="d-flex align-items-center">
                            <small class="text-muted"><a href="<?php echo base_url('/account/addachievement/'.$student['id']) ?>">Manage Achievement</a> </small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>

</div>

<?= $this->endsection() ?>