<?= $this->extend('app') ?>

<?= $this->section('content') ?>
    <div class="page-section bg-alt border-bottom-2">
        <div class="container page__container">

            <div class="d-flex flex-column flex-lg-row align-items-center">
                <div class="d-flex flex-column align-items-center align-items-lg-start flex mb-16pt mb-lg-0 text-center text-lg-left">
                    <h1 class="h2 mb-4pt">Go Kyo</h1>
                    <div class="lead measure-lead text-70">Teknik dalam olahraga Judo</div>
                </div>
                <div class="ml-lg-16pt">
                    <?php

                    $this->session = \Config\Services::session();
                    $role = $this->session->get('user_role');
                    if ($role == 'instructor') { ?>
                    <a href="<?php echo base_url('myprogram'); ?>" data-target="#library-drawer" data-toggle="sidebar" class="btn btn-light">
                        <i class="material-icons icon--left">tune</i> My Program
                        <span class="badge badge-notifications badge-accent icon--right">2</span>
                    </a>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>

    <div class="page-section border-bottom-2">
        <div class="container page__container">

            <?php if (!empty($search)){ ?>
                <div class="mb-32pt d-flex align-items-center">
                    <small class="text-black-70 text-headings text-uppercase mr-3">Displaying search result of "<?php echo $search ?>"</small>
                </div>
            <?php }
            if (count($programList) > 0) {
                foreach ($programList as $gokyo => $programs) { ?>
                <div class="page-separator">
                    <div class="page-separator__text"><?php echo $gokyo ?></div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row card-group-row">
                            <?php foreach ($programs as $program) { ?>
                                <div class="col-sm-6 col-xl-3 card-group-row__col">
                                    <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary-dodger-blue js-overlay mdk-reveal js-mdk-reveal card-group-row__card" data-partial-height="44" data-toggle="popover" data-trigger="click">
                                        <a href="<?php echo base_url('program/detail/'.$program['id']); ?>" class="js-image" data-position="">
                                            <img src="<?php echo base_url('/assets/uploads/'.$program['thumbnail']); ?>" alt="course" height="200px">
                                        </a>
                                        <div class="mdk-reveal__content">
                                            <div class="card-body">
                                                <div class="d-flex">
                                                    <div class="flex">
                                                        <a class="card-title" href="<?php echo base_url('program/detail/'.$program['id']); ?>"><?php echo $program['name'] ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>

            <?php
                }
            }elseif (count($programList) == 0){ ?>
                <div class="mb-32pt d-flex align-items-center">
                    <small class="text-black-70 text-headings text-uppercase mr-3">No Result</small>
                </div>
            <?php } ?>
        </div>
    </div>

<?= $this->endSection() ?>
