<?= $this->extend('app') ?>


<?= $this->section('css') ?>

<!-- Quill Theme -->
<link type="text/css" href="/assets/css/quill.css" rel="stylesheet">

<!-- Select2 -->
<link type="text/css" href="/assets/css/select2.css" rel="stylesheet">

<link type="text/css" href="/assets/vendor/select2/select2.min.css" rel="stylesheet">

<?= $this->endSection() ?>


<?= $this->section('content') ?>


    <div class="page-section bg-alt border-bottom-2">
        <div class="container page__container">

            <div class="d-flex flex-column flex-lg-row align-items-center">
                <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                    <h1 class="h2 mb-8pt">Edit Program</h1>
                </div>
                <div class="ml-lg-16pt">
                    <a href="<?php echo base_url('myprogram'); ?>" class="btn btn-light">Back to My Program</a>
                </div>
            </div>

        </div>
    </div>


    <div class="page-section border-bottom-2">
        <div class="container page__container">
            <form id="formId" action="<?php echo base_url('/myprogram/saveprogram'); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo @$dataFromDB['id'] ? @$dataFromDB['id'] : 0 ?>">
            <div class="row">
                <div class="col-md-8">

                    <div class="page-separator">
                        <div class="page-separator__text">Basic information</div>
                    </div>

                    <label class="form-label">Go Kyo</label>
                    <div class="form-group mb-24pt">
                        <select name="gokyo" class="form-control">
                            <?php foreach ($goKyoList as $go) { ?>
                                <option value="<?php echo $go ?>" <?php echo $go == @$dataFromDB['gokyo'] ? 'selected' : '' ?>><?php echo $go ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <label class="form-label">Program title</label>
                    <div class="form-group mb-24pt">
                        <input type="text" name="name" class="form-control form-control-lg" placeholder="Program Name" value="<?php echo @$dataFromDB['name'] ?>">
                    </div>

                    <div class="form-group mb-32pt">
                        <label class="form-label">Description</label>
                        <input type="hidden" id="description" name="description" value="<?php echo @$dataFromDB['description'] ?>">
                        <div style="height: 150px;" data-toggle="quill" data-quill-placeholder="Description">
                            <?php echo @$dataFromDB['description'] ?>
                        </div>
                    </div>

                    <div class="page-separator">
                        <div class="page-separator__text">Videos</div><button type="button" class="btn btn-primary" style="position: absolute;right: 0px;" onclick="addVideo()">Add Video</button>
                    </div>
                    <div id="videoList">

                    </div>
                    <input type="hidden" id="deletedVideo" name="deleted_video" value="">
                    <?php for ($i=0; $i<count($dataFromDB['videos']);$i++){ ?>
                        <div class="card" id="videoId-<?php echo $dataFromDB['videos'][$i]['id'] ?>">
                            <!--                                                        <div class="embed-responsive embed-responsive-16by9">-->
                            <!--                                                            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/97243285?title=0&amp;byline=0&amp;portrait=0" allowfullscreen=""></iframe>-->
                            <!--                                                        </div>-->
                            <div class="card-body">
                                <div class="form-group mb-12pt">
                                    <label class="form-label">Video Name</label>
                                    <input type="text" name="edit_video_name[]" class="form-control" value="<?php echo $dataFromDB['videos'][$i]['name'] ?>" placeholder="Video Name">
                                    <input type="hidden" name="edit_video_id[]" class="form-control" value="<?php echo $dataFromDB['videos'][$i]['id'] ?>">
                                </div>
                                <div class="form-group mb-12pt">
                                    <label class="form-label">Video File</label>
                                    <div class="custom-file">
                                        <input type="file" name="edit_video_file[]" id="file" class="form-control">
                                    </div>
                                </div>
                                <a href="#" class="text-red" style="color:red" onclick="removeRow('<?php echo $dataFromDB['videos'][$i]['id'] ?>')">Delete Video</a>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="page-separator">
                        <div class="page-separator__text">Syllabus</div><button type="button" class="btn btn-primary" style="position: absolute;right: 0px;" onclick="addSyllabus()">Add Syllabus</button>
                    </div>

                    <div id="syllabusList">

                    </div>
                    <input type="hidden" id="deletedSyllabus" name="deleted_syllabus" value="">
                    <?php for ($i=0; $i<count($dataFromDB['syllabus']);$i++){ ?>
                        <div class="card" id="syllabusId-<?php echo $dataFromDB['syllabus'][$i]['id'] ?>">
                            <div class="card-body">
                                <div class="form-group mb-12pt">
                                    <label class="form-label">Syllabus Name</label>
                                    <input type="hidden" name="edit_syllabus_id[]" value="<?php echo $dataFromDB['syllabus'][$i]['id'] ?>">
                                    <input type="text" name="edit_syllabus_name[]" class="form-control" value="<?php echo $dataFromDB['syllabus'][$i]['name'] ?>" placeholder="Enter Video URL">
                                </div>
                                <div class="form-group mb-12pt">
                                    <label class="form-label">Change Syllabus File</label>
                                    <div class="custom-file">
                                        <input type="file" name="edit_syllabus_file[]" id="file" class="custom-file-input">
                                        <label for="file" class="custom-file-label">Choose file</label>
                                    </div>
                                </div>
                                <a href="#" class="text-red" style="color:red" onclick="removeRow2('<?php echo $dataFromDB['syllabus'][$i]['id'] ?>')">Delete Syllabus</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-4">

                    <div class="card">
                        <div class="card-header text-center">
                            <button type="submit" class="btn btn-accent">Save changes</button>
                        </div>
                        <div class="list-group list-group-flush">
                            <?php if (isset($dataFromDB['id'])){?>
                                <div class="list-group-item">
                                    <a href="<?php echo base_url('/myprogram/removeprogram/'.$dataFromDB['id']) ?>" class="text-danger"><strong>Delete Course</strong></a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="page-separator">
                        <div class="page-separator__text">Thumbnail</div>
                    </div>

                    <div class="card">
                        <div class="embed-responsive embed-responsive-16by9">
                            <img class="embed-responsive-item" src="<?php echo @$dataFromDB['thumbnail'] ? base_url('/assets/uploads/'.$dataFromDB['thumbnail']) : '' ?>" allowfullscreen=""></img>
                        </div>
                        <div class="card-body">
                            <label class="form-label">Image</label>
                            <div class="custom-file">
                                <input type="file" name="thumbnail" id="file" class="form-control">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>



<?= $this->endSection() ?>

<?= $this->section('css') ?>

<link type="text/css" href="/assets/css/quill.css" rel="stylesheet">

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<!-- Quill -->
<script src="/assets/vendor/quill.min.js"></script>
<script src="/assets/js/quill.js"></script>
<!-- Select2 -->
<script src="/assets/vendor/select2/select2.min.js"></script>
<script src="/assets/js/select2.js"></script>

<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    $(document).ready(function(){
        $("#formId").on("submit", function () {
            $('#description').val($('.ql-editor').html());
        });
    });
    function addVideo() {
        const div = document.createElement('div');

        div.innerHTML = `
    <div class="card">
        <div class="card-body">
            <div class="form-group mb-12pt">
                <label class="form-label">Video Name</label>
                <input type="text" name="new_video_name[]" class="form-control" value="" placeholder="Video Name">
            </div>
            <div class="form-group mb-12pt">
                <label class="form-label">Video File</label>
                <div class="custom-file">
                    <input type="file" name="new_video_file[]" id="file" class="form-control">
                </div>
            </div>
        </div>
    </div>
  `;

        document.getElementById('videoList').appendChild(div);
    }


    function addSyllabus() {
        const divs = document.createElement('div');

        divs.innerHTML = `
        <div class="card">
            <div class="card-body">
                <div class="form-group mb-12pt">
                    <label class="form-label">Syllabus Name</label>
                    <input type="text" name="new_syllabus_name[]" class="form-control" value="" placeholder="Syllabus Name">
                </div>
                <div class="form-group mb-12pt">
                    <label class="form-label">Syllabus File</label>
                    <div class="custom-file">
                        <input type="file" name="new_syllabus_file[]" id="file" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    `;

        document.getElementById('syllabusList').appendChild(divs);
    }

    function removeRow(idDiv) {
        var valueBefore = $("#deletedVideo").val();
        if (valueBefore == ''){
            var valueAfter = idDiv;
        }else{
            var valueAfter = valueBefore + "," + idDiv;
        }
        $("#deletedVideo").val(valueAfter)
        $("#videoId-"+idDiv).remove();
    }

    function removeRow2(idDiv) {
        var valueBefore = $("#deletedSyllabus").val();
        if (valueBefore == ''){
            var valueAfter = idDiv;
        }else{
            var valueAfter = valueBefore + "," + idDiv;
        }
        $("#deletedSyllabus").val(valueAfter)
        $("#syllabusId-"+idDiv).remove();
    }
</script>
<?= $this->endSection() ?>



