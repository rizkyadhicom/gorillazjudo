<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap" rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="/assets/vendor/perfect-scrollbar.css" rel="stylesheet">

    <!-- Fix Footer CSS -->
    <link type="text/css" href="/assets/vendor/fix-footer.css" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="/assets/css/material-icons.css" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link type="text/css" href="/assets/css/fontawesome.css" rel="stylesheet">

    <!-- Preloader -->
    <link type="text/css" href="/assets/css/preloader.css" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="/assets/css/app.css" rel="stylesheet">


    <?= $this->renderSection('css') ?>
</head>


































<body class="layout-sticky layout-sticky-subnav ">

<div class="preloader">
    <div class="sk-double-bounce">
        <div class="sk-child sk-double-bounce1"></div>
        <div class="sk-child sk-double-bounce2"></div>
    </div>
</div>

<!-- Header Layout -->
<div class="mdk-header-layout js-mdk-header-layout">

    <!-- Header -->

    <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed>
        <div class="mdk-header__content">



            <div class="navbar navbar-expand pr-0 navbar-light bg-white navbar-shadow" id="default-navbar" data-primary>
                <!-- Navbar toggler -->
                <button class="navbar-toggler w-auto mr-16pt d-block d-lg-none rounded-0" type="button" data-toggle="sidebar">
                    <span class="material-icons">short_text</span>
                </button>

                <!-- Navbar Brand -->
                <a href="/admin" class="navbar-brand mr-16pt">

                        <span class="avatar avatar-lg navbar-brand-icon mr-0 mr-lg-8pt" style="padding: 4px">

                            <span class="rounded bg-primary"><img src="/assets/images/LEQ7eCHY.jpg" alt="logo" class="img-fluid"
                                                                  style="border-radius: 5px;" /></span>

                        </span>

                    <span class="d-none d-lg-block">GorillazJudo</span>
                </a>

                <div class="flex"></div>



                <div class="nav navbar-nav flex-nowrap d-flex mr-16pt">

                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown" data-caret="false">

                                <span class="avatar avatar-sm mr-8pt2">

                                    <span class="avatar-title rounded-circle bg-primary"><i class="material-icons">account_box</i></span>

                                </span>

                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-header"><strong>Account</strong></div>
                            <a class="dropdown-item" href="<?php echo base_url('/logout') ?>">Logout</a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>

    <!-- // END Header -->

    <!-- Header Layout Content -->
    <div class="mdk-header-layout__content">

        <!-- Drawer Layout -->
        <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">

            <!-- Drawer Layout Content -->
            <div class="mdk-drawer-layout__content page-content">



                <?= $this->renderSection('content') ?>



            </div>
            <!-- // END drawer-layout__content -->




            <!-- drawer -->
            <div class="mdk-drawer js-mdk-drawer" id="default-drawer">
                <div class="mdk-drawer__content top-navbar">
                    <div class="sidebar sidebar-dark-dodger-blue sidebar-left sidebar-p-t" data-perfect-scrollbar>








                        <div class="sidebar-heading">Administrator</div>
                        <ul class="sidebar-menu">


                            <li class="sidebar-menu-item active">
                                <a class="sidebar-menu-button" href="<?php echo base_url('/admin/dashboard'); ?>">
                                    <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>
                                    <span class="sidebar-menu-text">Dashboard</span>
                                </a>
                            </li>
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="<?php echo base_url('/admin/students'); ?>">
                                    <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>
                                    <span class="sidebar-menu-text">Manage Student</span>
                                </a>
                            </li>
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="<?php echo base_url('/admin/instructors'); ?>">
                                    <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>
                                    <span class="sidebar-menu-text">Manage Instructor</span>
                                </a>
                            </li>
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="<?php echo base_url('/admin/programs'); ?>">
                                    <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>
                                    <span class="sidebar-menu-text">Manage Program</span>
                                </a>
                            </li>
                            <li class="sidebar-menu-item">
                                <a class="sidebar-menu-button" href="<?php echo base_url('/admin/newslist'); ?>">
                                    <span class="material-icons sidebar-menu-icon sidebar-menu-icon--left">school</span>
                                    <span class="sidebar-menu-text">Manage News</span>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <!-- // END drawer -->

        </div>
        <!-- // END drawer-layout -->

    </div>
    <!-- // END Header Layout Content -->

</div>
<!-- // END Header Layout -->

<!-- jQuery -->
<script src="/assets/vendor/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/assets/vendor/popper.min.js"></script>
<script src="/assets/vendor/bootstrap.min.js"></script>

<!-- Perfect Scrollbar -->
<script src="/assets/vendor/perfect-scrollbar.min.js"></script>

<!-- DOM Factory -->
<script src="/assets/vendor/dom-factory.js"></script>

<!-- MDK -->
<script src="/assets/vendor/material-design-kit.js"></script>

<!-- Fix Footer -->
<script src="/assets/vendor/fix-footer.js"></script>

<!-- App JS -->
<script src="/assets/js/app.js"></script>


<!-- Global Settings -->
<script src="/assets/js/settings.js"></script>

<!-- Moment.js -->
<script src="/assets/vendor/moment.min.js"></script>
<script src="/assets/vendor/moment-range.min.js"></script>

<!-- Chart.js -->
<script src="/assets/vendor/Chart.min.js"></script>

<!-- UI Charts Page JS -->
<script src="/assets/js/chartjs-rounded-bar.js"></script>
<script src="/assets/js/chartjs.js"></script>

<!-- Chart.js Samples -->
<script src="/assets/js/page.instructor-dashboard.js"></script>

<!-- List.js -->
<script src="/assets/vendor/list.min.js"></script>
<script src="/assets/js/list.js"></script>


<?= $this->renderSection('js') ?>

<!-- App Settings (safe to remove) -->
<!-- <script src="assets/js/app-settings.js"></script> -->
</body>

</html>