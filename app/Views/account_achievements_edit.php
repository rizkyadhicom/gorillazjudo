<?= $this->extend('app') ?>



<?= $this->section('content') ?>

    <div class="container page__container page-section">
        <div class="page-separator">
            <div class="page-separator__text">Achievements</div><button type="button" class="btn btn-primary" style="position: absolute;right: 0px;" onclick="addAchievement()">Add Achievement</button>
        </div>
        <div class="col-md-12 p-0">
            <form action="<?php echo base_url('account/doachievement/'.$dataFromDB['id']); ?>" method="post">
                <div id="achievementList">

                </div>
                <?php if(!empty($dataFromDB['achievements'])) { foreach ($dataFromDB['achievements'] as $achievement){ ?>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group mb-12pt">
                            <input type="hidden" name="edit_achievement_id[]" value="<?php echo $achievement['id'] ?>"
                            <label class="form-label">Achievement Title</label>
                            <input type="text" name="edit_achievement_title[]" class="form-control" value="<?php echo $achievement['name'] ?>" placeholder="Achievement Title">
                        </div>
                        <div class="form-group mb-12pt">
                            <label class="form-label">Achievement Description</label>
                            <input type="text" name="edit_achievement_description[]" class="form-control" value="<?php echo $achievement['description'] ?>" placeholder="Achievement Description">
                        </div>
                        <div class="form-group mb-12pt">
                            <label class="form-label">Achievement Date</label>
                            <input type="date" name="edit_achievement_date[]" class="form-control" value="<?php echo $achievement['date'] ?>" placeholder="Achievement Date">
                        </div>
                        <a href="<?php echo base_url('account/removeachievement/'.$dataFromDB['id'].'/'.$achievement['id']); ?>" style="color:red">Remove Achievement</a>
                    </div>
                </div>
                <?php } } ?>
                <button class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div>

<?= $this->endsection() ?>

<?= $this->section('js') ?>
<script>
    function addAchievement() {
        const divs = document.createElement('div');

        divs.innerHTML = `
        <div class="card">
            <div class="card-body">
                <div class="form-group mb-12pt">
                    <label class="form-label">Achievement Title</label>
                    <input type="text" name="new_achievement_title[]" class="form-control" value="" placeholder="Achievement Title">
                </div>
                <div class="form-group mb-12pt">
                    <label class="form-label">Achievement Description</label>
                    <input type="text" name="new_achievement_description[]" class="form-control" value="" placeholder="Achievement Description">
                </div>
                <div class="form-group mb-12pt">
                    <label class="form-label">Achievement Date</label>
                    <input type="date" name="new_achievement_date[]" class="form-control" value="" placeholder="Achievement Date">
                </div>
<!--                <a href="#" onclick="removeRow(this)" style="color:red">Remove Achievement</a>-->
            </div>
        </div>
    `;

        document.getElementById('achievementList').appendChild(divs);
    }

</script>

<?= $this->endsection() ?>