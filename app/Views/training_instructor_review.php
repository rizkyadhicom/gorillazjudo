<<?= $this->extend('app') ?>

<?= $this->section('content') ?>
<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                <h1 class="h2 mb-8pt">Program: <?php echo $program['name'] ?></h1>
            </div>
        </div>

    </div>
</div>

<div class="container page__container">
    <div class="row">
        <div class="col-lg-8">

            <?php
                $count = 1;
                foreach ($training as $train) {
            ?>
            <div class="border-left-2 page-section pl-32pt">

                <div class="d-flex align-items-center page-num-container mb-16pt">
                    <div class="page-num"><?php echo $count ?></div>
                    <h4>Submited Training Result</h4>
                </div>

                <div class="form-group mb-12pt">
                    <label class="form-label">Training Result : <?php echo date('Y-m-d', strtotime($train['date'])) ?></label>
                    <div class="player__embed">
                        <video width="320" height="240" controls>
                            <source src="<?php echo base_url('/assets/uploads/'.@$train['video_url']) ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
<!--                        <iframe class="embed-responsive-item" src="" allowfullscreen=""></iframe>-->
                    </div>
                </div>
                <?php
                    if (!empty($train['comments'])){
                        foreach ($train['comments'] as $comment){

                ?>
                <div class="<?php echo $comment['user']['role'] == 'instructor' ? 'card card-body' : 'ml-sm-32pt mt-3 card p-3' ?>">
                    <div class="d-flex">
                        <a href="" class="avatar avatar-sm avatar-online mr-12pt">
                            <span class="avatar-title rounded-circle"><?php echo strtoupper(substr($comment['user']['name'],0,2)) ?></span>
                        </a>
                        <div class="flex">
                            <p class="d-flex align-items-center mb-2">
                                <a href="" class="text-body mr-2"><strong><?php echo $comment['user']['name'] ?></strong></a>
                                <small class="text-muted">2 min ago</small>
                            </p>
                            <p><?php echo $comment['comment'] ?></p>
                        </div>
                    </div>
                </div>
                <?php }} ?>
                <div class="d-flex mb-4">
                    <a href="" class="avatar avatar-sm mr-12pt">
                        <!-- <img src="assets/images/people/50/guy-6.jpg" alt="people" class="avatar-img rounded-circle"> -->
                        <span class="avatar-title rounded-circle">GJ</span>
                    </a>
                    <div class="flex">
                        <form action="/training/postcomment/<?php echo $train['id'] ?>" method="post">
                            <div class="form-group">
                                <label for="comment" class="form-label">Your Comment</label>
                                <textarea class="form-control" name="comment" id="comment" rows="3" placeholder="Type here to reply ..."></textarea>
                            </div>
                            <button class="btn btn-outline-secondary" type="submit">Post comment</button>
                        </form>
                    </div>
                </div>
                <div class="card card-body">
                    <h3>Penilaian</h3>
                    <form action="/training/postscore/<?php echo $train['id']?>" method="post">
                    <div class="form-group col-md-6">
                        <label for="touch-spin-2" class="form-label">Score</label>
                        <input id="touch-spin-2" name="score" data-toggle="touch-spin" data-min="0" data-max="100" data-step="1" type="text" value="<?php echo $train['score'] ? $train['score'] : '0' ?>" class="form-control" />
                    </div>
                        <button class="btn btn-primary"><?php echo $train['status'] == "COMPLETED" ? 'Update Score' : 'Submit Score' ?></button>
                        <?php if($train['status'] == "REVIEW"){ ?>
                        <button class="btn btn-danger" style="float: right">Reject</button>
                        <?php } ?>
                    </form>
                </div>

            </div>
            <?php
                    $count++;
                }
            ?>

        </div>
        <div class="col-lg-4 page-section">

            <div class="d-flex flex-column mb-24pt">
                <a href="<?php echo base_url('/program/detail/'.$program['id'])?>" class="btn justify-content-center btn-accent  mb-16pt">See Program <i class="material-icons icon--right">keyboard_arrow_right</i></a>
            </div>

            <div class="page-separator">
                <div class="page-separator__text">Program</div>
            </div>

            <a href="<?php echo base_url('/program/detail/'.$program['id'])?>" class="d-flex flex-nowrap mb-24pt">
                            <span class="mr-16pt">
                                <img src="<?php echo base_url('/assets/uploads/'.$program['thumbnail']) ?>" width="40" alt="<?php echo $program['name'] ?>" class="rounded">
                            </span>
                <span class="flex d-flex flex-column align-items-start">
                                <span class="card-title"><?php echo $program['name'] ?></span>
                                <span class="card-subtitle text-50"><?php echo $program['countVideos'] ?> Videos</span>
                            </span>
            </a>

            <div class="page-separator">
                <div class="page-separator__text">Student</div>
            </div>

            <div class="accordion__item open">
                <div class="accordion__menu collapse show" id="course-toc-2">
                    <div class="accordion__menu-link" style="padding-left: 0rem !important;">
                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">person</i>
                                            </span>
                        <span class="flex"><?php echo $student['name'] ? $student['name'] : '-' ?></span>
                    </div>
                    <div class="accordion__menu-link" style="padding-left: 0rem !important;">
                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">school</i>
                                            </span>
                        <span class="flex"><?php echo $student['school_name'] ? $student['school_name'] : '-' ?></span>
                    </div>
                    <div class="accordion__menu-link" style="padding-left: 0rem !important;">
                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">alternate_email</i>
                                            </span>
                        <span class="flex"><?php echo $student['email'] ?></span>
                    </div>
                    <div class="accordion__menu-link" style="padding-left: 0rem !important;">
                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">local_phone</i>
                                            </span>
                        <span class="flex"><?php echo $student['phone'] ? $student['phone'] : '-' ?></span>
                    </div>
                    <div class="accordion__menu-link" style="padding-left: 0rem !important;">
                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">place</i>
                                            </span>
                        <span class="flex"><?php echo $student['address'] ? $student['address'] : '-' ?></span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?= $this->endSection() ?>


<?= $this->section('js') ?>
<!-- Touchspin -->
<script src="/assets/vendor/jquery.bootstrap-touchspin.js"></script>
<script src="/assets/js/touchspin.js"></script>

<?= $this->endSection() ?>