<?= $this->extend('app') ?>


<?= $this->section('css') ?>

<!-- Quill Theme -->
<link type="text/css" href="/assets/css/quill.css" rel="stylesheet">

<!-- Select2 -->
<link type="text/css" href="/assets/css/select2.css" rel="stylesheet">

<link type="text/css" href="/assets/vendor/select2/select2.min.css" rel="stylesheet">

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<!-- Quill -->
<script src="/assets/vendor/quill.min.js"></script>
<script src="/assets/js/quill.js"></script>
<!-- Select2 -->
<script src="/assets/vendor/select2/select2.min.js"></script>
<script src="/assets/js/select2.js"></script>

<?= $this->endSection() ?>


<?= $this->section('content') ?>


    <div class="page-section bg-alt border-bottom-2">
        <div class="container page__container">

            <div class="d-flex flex-column flex-lg-row align-items-center">
                <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                    <h1 class="h2 mb-8pt">Edit Program</h1>
                </div>
                <div class="ml-lg-16pt">
                    <a href="<?php echo base_url('myprogram'); ?>" class="btn btn-light">Back to My Program</a>
                </div>
            </div>

        </div>
    </div>


    <div class="page-section border-bottom-2">
        <div class="container page__container">

            <div class="row">
                <div class="col-md-8">

                    <div class="page-separator">
                        <div class="page-separator__text">Basic information</div>
                    </div>

                    <label class="form-label">Program title</label>
                    <div class="form-group mb-24pt">
                        <input type="text" class="form-control form-control-lg" placeholder="Course title" value="Angular Fundamentals">
                    </div>
                    <div class="form-group mb-32pt">
                        <label class="form-label">Program Thumbnail</label>
                        <div class="custom-file">
                            <input type="file" id="file" class="custom-file-input">
                            <label for="file" class="custom-file-label">Choose file</label>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">

                    <div class="card">
                        <div class="card-header text-center">
                            <a href="#" class="btn btn-accent">Save changes</a>
                        </div>
                        <div class="list-group list-group-flush">
                            <div class="list-group-item d-flex">
                                <a class="flex" href="#"><strong>Save Draft</strong></a>
                                <i class="material-icons text-muted">check</i>
                            </div>
                            <div class="list-group-item">
                                <a href="#" class="text-danger"><strong>Delete Course</strong></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



<?= $this->endSection() ?>
