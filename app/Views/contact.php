<?= $this->extend('app') ?>

<?= $this->section('content') ?>


<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column align-items-center align-items-lg-start flex text-center text-lg-left">
            <h1 class="h2 mb-4pt">Contact Us</h1>
        </div>

    </div>
</div>








<div class="page-section bg-body border-bottom-2">
    <div class="container page__container">


        <div class="row card-group-row">


            <div class="col-lg-6">


                <label class="form-label">Name</label>
                <div class="form-group mb-24pt">
                    <input type="text" name="name" class="form-control form-control-lg" placeholder="Name" value="">
                </div>

                <label class="form-label">Email</label>
                <div class="form-group mb-24pt">
                    <input type="text" name="name" class="form-control form-control-lg" placeholder="Email" value="">
                </div>

                <div class="form-group mb-32pt">
                    <label class="form-label">Messages</label>
                    <div style="height: 150px;" data-toggle="quill" data-quill-placeholder="Description"></div>
                </div>

                <button class="btn btn-primary">Send</button>
            </div>


            <div class="col-lg-6">
                <h4>Location</h4>
                <div class="col-md-8">
                    <iframe
                            width="300"
                            height="300"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD9RUNa-NOfKGJsQeh8YtvQHxLDrprVNXU
    &q=Gorillaz+Judo+Bogor" allowfullscreen>
                    </iframe>
                </div>

            </div>
        </div>


    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('css') ?>

<link type="text/css" href="/assets/css/quill.css" rel="stylesheet">

<?= $this->endSection() ?>
<?= $this->section('js') ?>

<!-- Quill -->
<script src="/assets/vendor/quill.min.js"></script>
<script src="/assets/js/quill.js"></script>
<!-- Select2 -->
<script src="/assets/vendor/select2/select2.min.js"></script>
<script src="/assets/js/select2.js"></script>

<?= $this->endSection() ?>

