<?= $this->extend('cms_app') ?>

<?= $this->section('content') ?>



<div class="pt-32pt">
    <div class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-sm-left">
        <div class="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">

            <div class="mb-24pt mb-sm-0 mr-sm-24pt">
                <h2 class="mb-0">Dashboard</h2>

                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>

                    <li class="breadcrumb-item active">

                        Manage News

                    </li>

                </ol>

            </div>
        </div>

        <div class="row" role="tablist">
            <div class="col-auto">
                <a href="<?php echo base_url('/admin/news/0') ?>" class="btn btn-outline-secondary">Add News</a>
            </div>
        </div>
    </div>
</div>


<div class="page-section border-bottom-2">
    <div class="container page__container">

        <div class="page-separator">
            <div class="page-separator__text">News</div>
        </div>

        <div class="card mb-lg-32pt">

            <div class="table-responsive" data-toggle="lists" data-lists-sort-by="js-lists-values-date" data-lists-sort-desc="true" data-lists-values='["js-lists-values-name", "js-lists-values-company", "js-lists-values-phone", "js-lists-values-date"]'>

                <table class="table mb-0 thead-border-top-0 table-nowrap">
                    <thead>
                        <tr>
                            <th>News Title</th>
                            <th style="width: 37px;">Publish Date</th>
                            <th style="width: 24px;"></th>
                        </tr>
                    </thead>
                    <tbody class="list" id="clients">
                    <?php foreach ($dataFromDB as $row){?>
                        <tr>
                            <td>
                                <div class="media flex-nowrap align-items-center" style="white-space: nowrap;">
                                    <div class="avatar avatar-sm mr-8pt">
                                        <span class="avatar-title rounded-circle"><?php echo strtoupper(substr($row['title'],0,2)) ?></span>
                                    </div>
                                    <div class="media-body">
                                        <div class="d-flex flex-column">
                                            <p class="mb-0"><strong class="js-lists-values-name"><?php echo $row['title'] ?></strong></p>
<!--                                            <small class="js-lists-values-email text-50"></small>-->
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td><strong class="js-lists-values-lead"><?php echo $row['publish_date'] ?></strong></td>

                            <td class="text-right">
                                <a href="<?php echo base_url('/admin/news/'.$row['id']) ?>" class="text-50"><i class="material-icons">more_vert</i></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>

            <div class="card-footer p-8pt">
                <ul class="pagination justify-content-start pagination-xsm m-0">
                    <li class="page-item <?php if ($page == 1) echo 'disabled' ?>">
                        <a class="page-link" href="?page=<?php echo $page-1 ?>" aria-label="Previous">
                            <span aria-hidden="true" class="material-icons">chevron_left</span>
                            <span>Prev</span>
                        </a>
                    </li>
                    <?php for ($i=1; $i<=$totalPage; $i++) { ?>
                        <li class="page-item">
                            <a class="page-link" href="?page=<?php echo $i ?>" aria-label="Page <?php echo $i ?>">
                                <span><?php echo $i ?></span>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="page-item <?php if ($page == $totalPage) echo 'disabled' ?>">
                        <a class="page-link" href="?page=<?php echo $page+1 ?>" aria-label="Next">
                            <span>Next</span>
                            <span aria-hidden="true" class="material-icons">chevron_right</span>
                        </a>
                    </li>
                </ul>

            </div>


        </div>

    </div>
</div>



<?= $this->endSection() ?>