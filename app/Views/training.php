<<?= $this->extend('app') ?>

<?= $this->section('content') ?>
<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                <?php
                    $score = "Waiting for Submit";
                    $submitedDate = "Not Submited Yet";
                    if ($lastStatus['status'] == 'REVIEW'){
                        $score = 'In Review';
                        $submitedDate = date('Y-m-d', strtotime($lastStatus['submited_date']));
                    }elseif($lastStatus['status'] == 'COMPLETED'){
                        $score = $lastStatus['score'];
                        $submitedDate = date('Y-m-d', strtotime($lastStatus['submited_date']));
                    }
                ?>

                <h1 class="h2 mb-8pt">Your Score: <?php echo $score ?></h1>
                <div>
                    <span class="chip chip-outline-secondary d-inline-flex align-items-center">
                        <i class="material-icons icon--left">event</i>
                        <?php echo $submitedDate ?>
                    </span>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container page__container">
    <div class="row">
        <div class="col-lg-8">

            <?php
                $count = 1;
                foreach ($training as $train) {
            ?>
            <div class="border-left-2 page-section pl-32pt">

                <div class="d-flex align-items-center page-num-container mb-16pt">
                    <div class="page-num"><?php echo $count ?></div>
                    <h4>Submited Training Result</h4>
                </div>

                <div class="form-group mb-12pt">
                    <label class="form-label">Training Result : <?php echo date('Y-m-d', strtotime($train['date'])) ?></label>
                    <div class="player__embed">
                        <video width="320" height="240" controls>
                            <source src="<?php echo base_url('/assets/uploads/'.@$train['video_url']) ?>" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
<!--                        <iframe class="embed-responsive-item" src="" allowfullscreen=""></iframe>-->
                    </div>
                </div>
                <?php
                if (!empty($train['comments'])){
                    foreach ($train['comments'] as $comment){

                        ?>
                        <div class="<?php echo $comment['user']['role'] == 'instructor' ? 'card card-body' : 'ml-sm-32pt mt-3 card p-3' ?>">
                            <div class="d-flex">
                                <a href="" class="avatar avatar-sm avatar-online mr-12pt">
                                    <span class="avatar-title rounded-circle"><?php echo strtoupper(substr($comment['user']['name'],0,2)) ?></span>
                                </a>
                                <div class="flex">
                                    <p class="d-flex align-items-center mb-2">
                                        <a href="" class="text-body mr-2"><strong><?php echo $comment['user']['name'] ?></strong></a>
                                        <small class="text-muted"><?php echo date('Y-m-d H:i:s', strtotime($comment['date'])) ?></small>
                                    </p>
                                    <p><?php echo $comment['comment'] ?></p>
                                </div>
                            </div>
                        </div>
                    <?php }} ?>
                <div class="d-flex mb-4">
                    <a href="" class="avatar avatar-sm mr-12pt">
                        <!-- <img src="assets/images/people/50/guy-6.jpg" alt="people" class="avatar-img rounded-circle"> -->
                        <span class="avatar-title rounded-circle">GJ</span>
                    </a>
                    <div class="flex">
                        <form action="/training/postcomment/<?php echo $train['id'] ?>" method="post">
                            <div class="form-group">
                                <label for="comment" class="form-label">Your Comment</label>
                                <textarea class="form-control" name="comment" id="comment" rows="3" placeholder="Type here to reply ..."></textarea>
                            </div>
                            <button class="btn btn-outline-secondary" type="submit">Post comment</button>
                        </form>
                    </div>
                </div>

            </div>
            <?php
                    $count++;
                }
            ?>
            <div class="border-left-2 page-section pl-32pt">

                <div class="d-flex align-items-center page-num-container mb-16pt">
                    <div class="page-num"><?php echo $count ?></div>
                    <h4>Upload Video File</h4>
                </div>

                <div class="form-group mb-12pt">
                    <label class="form-label">Training Result</label>
                    <form id="formUpload" action="<?php echo base_url('/training/submit/'.$program['id']) ?>" method="post" enctype="multipart/form-data">
                        <div class="custom-file">
                            <input type="file" name="training_result" id="file" class="form-control" required>
                        </div>
                        <br>
                        <br>
                        <div class="form-group">
                            <label for="training_date" class="form-label">Date Training</label>
                            <input type="date" name="training_date" id="training_date" class="form-control" value="<?php echo date("Y-m-d") ?>" max="<?php echo date("Y-m-d") ?>" required>
                        </div>
                    </form>
                </div>


            </div>

        </div>
        <div class="col-lg-4 page-section">

            <div class="d-flex flex-column mb-24pt">
                <a href="<?php echo base_url('/program/detail/'.$program['id'])?>" class="btn justify-content-center btn-outline-secondary mb-16pt">Watch Video Again</a>
                <button type="button" onclick="submitUpload()" class="btn justify-content-center btn-accent ">Submit your training result <i class="material-icons icon--right">keyboard_arrow_right</i></button>
            </div>

            <div class="page-separator">
                <div class="page-separator__text">Program</div>
            </div>

            <a href="<?php echo base_url('/program/detail/'.$program['id'])?>" class="d-flex flex-nowrap mb-24pt">
                            <span class="mr-16pt">
                                <img src="<?php echo base_url('/assets/uploads/'.$program['thumbnail']) ?>" width="40" alt="<?php echo $program['name'] ?>" class="rounded">
                            </span>
                <span class="flex d-flex flex-column align-items-start">
                                <span class="card-title"><?php echo $program['name'] ?></span>
                                <span class="card-subtitle text-50"><?php echo $program['countVideos'] ?> Videos</span>
                            </span>
            </a>

        </div>
    </div>
</div>

<?= $this->endSection() ?>



<?= $this->section('js') ?>
<script type="application/javascript">
    function submitUpload(){
        $('#formUpload').submit();
    }

</script>

<?= $this->endSection() ?>