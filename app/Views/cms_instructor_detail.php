<?= $this->extend('cms_app') ?>

<?= $this->section('content') ?>



<div class="pt-32pt">
    <div class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-sm-left">
        <div class="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">

            <div class="mb-24pt mb-sm-0 mr-sm-24pt">
                <h2 class="mb-0">Dashboard</h2>

                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>

                    <li class="breadcrumb-item active">

                        Manage Instructors

                    </li>

                </ol>

            </div>
        </div>

    </div>
</div>


<div class="page-section border-bottom-2">
    <div class="container page__container">

        <div class="page-separator">
            <div class="page-separator__text">Basic Information</div>
        </div>

        <div class="card mb-lg-32pt">


            <div class="container-fluid page__container page-section">

                <form action="<?php echo base_url('admin/saveinstructor'); ?>" method="post">
                    <input type="hidden" name="id" value="<?php echo @$dataFromDB['id'] ? @$dataFromDB['id'] : 0 ?>">
                    <input type="hidden" name="role" value="<?php echo @$dataFromDB['role'] ? @$dataFromDB['role'] : 'instructor' ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Nama Lengkap</label>
                                <input type="text" name="name" class="form-control" value="<?php echo @$dataFromDB['name'] ?>" placeholder="Masukkan Nama">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Tempat Tanggal Lahir</label>
                                <input type="date" name="birthday" class="form-control" value="<?php echo @$dataFromDB['birthday'] ? date('Y-m-d', strtotime(@$dataFromDB['birthday'])) : date('Y-m-d', strtotime('today')) ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Email</label>
                                <input type="text" name="email" class="form-control" value="<?php echo @$dataFromDB['email'] ?>" placeholder="Masukkan Email">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">No HP</label>
                                <input type="text" name="phone" class="form-control" value="<?php echo @$dataFromDB['phone'] ?>" placeholder="62 8123">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Alamat</label>
                                <textarea class="form-control" name="address" placeholder="Masukkan Alamat" name="address"><?php echo @$dataFromDB['address'] ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label">Jenis Kelamin</label>
                                <select name="gender" class="form-control">
                                    <option value="L" <?php echo @$dataFromDB['gender'] == 'L' ? 'selected' : '' ?>>Laki Laki</option>
                                    <option value="P" <?php echo @$dataFromDB['gender'] == 'P' ? 'selected' : '' ?>>Perempuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" value="" placeholder="Change password">
                            </div>
                        </div>
                    </div>
<!--                    <div class="row">-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="form-group">-->
<!--                                <label class="form-label">Confirm Password</label>-->
<!--                                <input type="password" name="repassword" class="form-control" value="" placeholder="Confirm Password">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->

                    <br>
                    <button class="btn btn-primary">Save changes</button>
                    <?php if (!empty($dataFromDB['id'])){ ?>
                        <a href="<?php echo base_url('/admin/removeinstructor/'.$dataFromDB['id'])?>" class="btn btn-danger">Delete</a>
                    <?php } ?>
                </form>
            </div>



        </div>

    </div>
</div>



<?= $this->endSection() ?>