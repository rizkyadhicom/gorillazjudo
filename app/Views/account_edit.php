<?= $this->extend('app') ?>



<?= $this->section('content') ?>

    <div class="container page__container page-section">
        <div class="page-separator">
            <div class="page-separator__text">Basic Information</div>
        </div>
        <div class="col-md-12 p-0">
            <form action="<?php echo base_url('account/dovalidate'); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $dataFromDB['id'] ?>">
                <div class="form-group">
                    <label class="form-label">Your photo</label>
                    <div class="media align-items-center">
                        <a href="" class="media-left mr-16pt">
                            <img src="<?php echo $dataFromDB['profile_image'] ? '/assets/uploads/'.$dataFromDB['profile_image'] : '/assets/images/people/110/guy-3.jpg' ?>" alt="people" width="56" class="rounded-circle" />
                        </a>
                        <div class="media-body">
                            <div class="custom-file">
                                <input type="file"name="profile_image" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Nama Lengkap:</label>
                        <div class="input-group input-group-merge">
                            <input id="name_2" name="name" value="<?php echo $dataFromDB['name'] ?>" type="text" required class="form-control form-control-prepended" placeholder="Masukkan Nama">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-user"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Tempat Tanggal Lahir:</label>
                        <div class="input-group input-group-merge">
                            <input id="name_2" name="birthday" value="<?php echo date('Y-m-d', strtotime($dataFromDB['birthday'])) ?>" type="date" required class="form-control form-control-prepended" placeholder="Masukkan Nama">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="fa fa-calendar"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Nama Orang Tua:</label>
                        <div class="input-group input-group-merge">
                            <input id="name_2" name="parent_name" value="<?php echo $dataFromDB['parent_name'] ?>" type="text" required class="form-control form-control-prepended" placeholder="Masukkan Nama Orang Tua">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-user"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">No HP:</label>
                        <div class="input-group input-group-merge">
                            <input id="name_2" name="phone" value="<?php echo $dataFromDB['phone'] ?>" type="text"  class="form-control" placeholder="62 8123" data-mask="(62)00000000000">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="fa fa-phone-alt"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="email_2">Email:</label>
                        <div class="input-group input-group-merge">
                            <input id="email_2" name="email" value="<?php echo $dataFromDB['email'] ?>" type="email" required class="form-control form-control-prepended" placeholder="Masukkan Email, Contoh email@example.com">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Jenis Kelamin:</label>
                        <div class="input-group input-group-merge">
                            <select name="gender" class="form-control">
                                <option value="L">Laki Laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Alamat:</label>
                        <div class="input-group input-group-merge">
                            <textarea class="form-control" placeholder="Masukkan Alamat" name="address"><?php echo $dataFromDB['address'] ?></textarea>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 mb-3">
                        <label class="text-label" for="name_2">Sekolah:</label>
                        <div class="input-group input-group-merge">
                            <input id="name_2" name="school_name" value="<?php echo $dataFromDB['school_name'] ?>" type="text" required class="form-control form-control-prepended" placeholder="Masukkan Nama Sekolah">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="fa fa-school"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="text-label" for="password_2">Change Password:</label>
                    <div class="input-group input-group-merge">
                        <input id="password_2" name="password" type="password" class="form-control form-control-prepended" placeholder="Ganti Kata Sandi" minlength="6">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-label" for="password_2">Confirm Password:</label>
                    <div class="input-group input-group-merge">
                        <input id="password_2" name="repassword" type="password" class="form-control form-control-prepended" placeholder="Ganti Kata Sandi" minlength="6">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fa fa-key"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary">Save changes</button>
            </form>
        </div>
    </div>

<?= $this->endsection() ?>