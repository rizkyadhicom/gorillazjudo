<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap" rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="assets/vendor/perfect-scrollbar.css" rel="stylesheet">

    <!-- Fix Footer CSS -->
    <link type="text/css" href="assets/vendor/fix-footer.css" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="assets/css/material-icons.css" rel="stylesheet">


    <!-- Font Awesome Icons -->
    <link type="text/css" href="assets/css/fontawesome.css" rel="stylesheet">


    <!-- Preloader -->
    <link type="text/css" href="assets/css/preloader.css" rel="stylesheet">


    <!-- App CSS -->
    <link type="text/css" href="assets/css/app.css" rel="stylesheet">






</head>





<body class="layout-default layout-login-centered-boxed">








<div class="layout-login-centered-boxed__form card">
    <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
        <a href="<?php echo base_url('/'); ?>" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">

                <span class="avatar avatar-lg navbar-brand-icon mr-0" style="padding: 4px">

                    <span class="rounded" ><img src="assets/images/logo/gorillaz.jpeg" alt="logo" class="img-fluid"  style="border-radius: 5px;"/></span>

                </span>

            Gorillaz Judo
        </a>
        <p class="m-0">Login to access Admin CMS </p>
    </div>
    <?php
    $this->session = \Config\Services::session();
    if($msg = $this->session->getFlashdata('error_login')){ ?>
        <div class="alert alert-soft-success d-flex" role="alert">
            <i class="material-icons mr-12pt">check_circle</i>
            <div class="text-body"><?= $msg ?></div>
        </div>
    <?php } else if($msg = $this->session->getFlashdata('success_login')){ ?>
        <div class="alert alert-soft-success d-flex" role="alert">
            <i class="material-icons mr-12pt">check_circle</i>
            <div class="text-body"><?= $msg ?></div>
        </div>
    <?php } ?>


    <form action="<?php echo base_url('admin/dovalidate'); ?>" method="post">
        <div class="form-group">
            <label class="text-label" for="email_2">Email Address:</label>
            <div class="input-group input-group-merge">
                <input id="email_2" name="email" type="email" required class="form-control form-control-prepended" placeholder="john@doe.com">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <span class="far fa-envelope"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="text-label" for="password_2">Password:</label>
            <div class="input-group input-group-merge">
                <input id="password_2" name="password" type="password" required class="form-control form-control-prepended" placeholder="Enter your password">
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <span class="fa fa-key"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-block btn-primary" type="submit">Login</button>
        </div>
        <div class="form-group text-center">
            Don't have an account? <a class="text-body text-underline" href="<?php echo base_url('/register'); ?>">Sign up!</a>
        </div>
    </form>
</div>


<!-- jQuery -->
<script src="assets/vendor/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="assets/vendor/popper.min.js"></script>
<script src="assets/vendor/bootstrap.min.js"></script>

<!-- Perfect Scrollbar -->
<script src="assets/vendor/perfect-scrollbar.min.js"></script>

<!-- DOM Factory -->
<script src="assets/vendor/dom-factory.js"></script>

<!-- MDK -->
<script src="assets/vendor/material-design-kit.js"></script>

<!-- Fix Footer -->
<script src="assets/vendor/fix-footer.js"></script>

<!-- App JS -->
<script src="assets/js/app.js"></script>




</body>

</html>