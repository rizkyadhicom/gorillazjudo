<?= $this->extend('app') ?>

<?= $this->section('content') ?>
    <div class="mdk-box mdk-box--bg-white-35 bg-white js-mdk-box mb-0" data-effects="parallax-background blend-background">
        <div class="mdk-box__bg">
            <div class="mdk-box__bg-front" style="background-image: url(assets/logo2.jpeg);background-position: right 10px;"></div>
        </div>
        <div class="mdk-box__content d-flex align-items-center justify-content-center container page__container text-center py-112pt" style="min-height: 656px;">
            <div class="card card--transparent mb-0">
                <div class="card-body px-32pt py-24pt">
                    <h2>Berlatih, Bermain, Bergembira, dan Berprestasi</h2>
                    <h2>Bersama</h2>
<!--                    <h1>Gorillaz Judo</h1>-->
<!--                    <p class="lead measure-lead mx-auto mb-32pt">Business, Technology and Creative Skills taught by industry experts. Explore a wide range of skills with our professional tutorials.</p>-->
                    <h2>Gorillaz Judo Club Kota Bogor</h2>
                    <a href="<?php echo base_url('/program'); ?>" class="btn btn-lg btn-accent btn--raised mb-16pt">Start Training!</a>
<!--                    <p class="mb-0"><a href=""><strong>Are you a teacher?</strong></a></p>-->

                </div>
            </div>
        </div>
    </div>









    <div class="page-section border-bottom-2 bg-white">
        <div class="container page__container">
            <div class="page-headline text-center">
                <h2>What Are You Looking For?</h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="d-flex col-md align-items-center border-bottom border-md-0  ml-auto mr-auto pb-16pt pb-md-0">
                        <div class="rounded-circle bg-dark w-64 h-64 d-inline-flex align-items-center justify-content-center  ml-auto mr-auto">
                            <i class="material-icons text-white text-center">sports</i>
                        </div>
                    </div>
                    <div class="flex">
                        <a href="<?php echo base_url('/program') ?>"> <div class="card-title text-center">Training Zone</div></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="d-flex col-md align-items-center border-bottom border-md-0  ml-auto mr-auto pb-16pt pb-md-0">
                        <div class="rounded-circle bg-dark w-64 h-64 d-inline-flex align-items-center justify-content-center  ml-auto mr-auto">
                            <i class="material-icons text-white text-center">event</i>
                        </div>
                    </div>
                    <div class="flex">
                        <a href="<?php echo base_url('/news') ?>"> <div class="card-title text-center">Latest News</div></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="page-section border-bottom-2 bg-white">
        <div class="container page__container">
            <div class="page-headline text-center">
                <h4>BERLATIH, BERMAIN BERGEMBIRA & BERPRESTASI</h4>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div id="lipsum" class="text-70 small mb-0">
                        <p>
                            Bakat dan kemampuan seseorang sudah dimulai sejak
                            masih kanak-kanak, jika ingin mewujudkan bakat, kemampuan serta
                            kecerdasan anak memang perlu dimulai sejak anak masih berusia dini. Karena pada usia dini lah kecerdasan dan fungsi-fungsi
                            mental lainnya, termasuk kreativitas berkembang paling cepat.
                            Anak-anak sebagaimana orang dewasa bisa bertindak dan melakukan
                            semua hal dengan sangat baik ketika merasa bahagia dan bergembira. Oleh karna itu sangat penting bagi seorang  Pelatih Klub JUDO  yang melakukan Pembinaan Usia Dini untuk menerapkan Konsep <b>"BERLATIH, BERMAIN, BERGEMBIRA & BERPRESTASI"</b>, dengan tujuan agar anak- anak merasa bahagia dan bergembira dalam berlatih sehingga tergali potensi dirinya dan dapat berprestasi. Ketika anak-anak  merasa bahagia dan bergembira maka anak-anak akan menyukai bahkan mencintai Olahraga Judo, dan ketika mereka mencintai Judo maka mereka akan sangat semangat & rajin berlatih sehingga pelatih akan dengan sangat mudah memberikan pelajaran tehnik-tehnik serta dalam  membentuk mental Juara kepada mereka. Olahraga JUDO
                            yang ditekuni sejak anak-anak akan membawa dampak yang bagus untuk
                            perkembangan kemampuan anak-anak. Maka saat itulah
                            pentingnya latihan yang teratur dan terarah.
                            Proses berlatih terkadang membosankan bagi anak-anak, tidak adanya
                            daya kreatifitas dan inovasi dari seorang  Pelatih Klub sehinggga membuat anak-anak berhenti
                            berlatih sewaktu-waktu, terkadang sasaran dan tujuan dari berlatih belum
                            tercapai anak-anak sudah berhenti berlatih karena merasa bosan. Oleh karna itu
                            latihan yang memfokuskan pada tehnik dasar  dan tehnik bertanding saja akan
                            membuat anak-anak  berhenti berlatih. Maka seorang Pelatih Klub dituntut harus mampu membuat materi Latihan yang dikemas dalam  permainan yang menarik
                            dan menyenangkan untuk anak-anak, yang mana hal tersebut merupakan salah satu jalan untuk
                            memberikan kegembiraan kepada anak-anak dalam berlatih olahraga Judo khususnya, dan membuatnya mencintai Judo hingga akhirnya mampu berprestasi.
                        </p>
                        <p>
                            <b>Founder of Gorillaz Judo Fighter</b>
                            <br>
                            Een Mardani
                        </p>
                    </div>
                </div>

            </div>

        </div>
    </div>

<?= $this->endSection() ?>
