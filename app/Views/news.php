<?= $this->extend('app') ?>

<?= $this->section('content') ?>


<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column align-items-center align-items-lg-start flex text-center text-lg-left">
            <h1 class="h2 mb-4pt">News</h1>
            <div class="lead measure-lead text-70">Kolom berita dan jadwal pertandingan</div>
        </div>

    </div>
</div>








<div class="page-section bg-body border-bottom-2">
    <div class="container page__container">

        <div class="text-center d-flex align-items-center flex-wrap mb-32pt" style="white-space: nowrap;">
            <h5 class="mr-24pt mb-md-0 d-md-inline-block">Topics</h5>
            <a href="" class="chip mb-16pt mb-md-0 chip-secondary">News</a>
            <a href="" class="chip mb-16pt mb-md-0 chip-outline-secondary">Pertandingan</a>
        </div>



        <div class="row card-group-row">


            <div class="col-lg-12">

                <div class="page-separator">
                    <div class="page-separator__text">Latest News</div>
                </div>
                <?php foreach ($news as $n) { ?>
                <div class="mb-8pt d-flex align-items-center">
                    <a href="<?php echo base_url('/news/detail/'.$n['id']) ?>" class="avatar avatar-lg overlay overlay--primary mr-12pt">
                        <img src="/assets/uploads/<?php echo $n['cover_image']?>" alt="/assets/uploads/<?php echo $n['cover_image']?>" class="avatar-img rounded">
                        <span class="overlay__content"></span>
                    </a>
                    <div class="flex">
                        <a class="card-title mb-4pt" href="<?php echo base_url('/news/detail/'.$n['id']) ?>"><?php echo $n['title'] ?></a>
                        <div class="d-flex align-items-center">
                            <small class="text-muted mr-8pt"><?php echo $n['author'] ?></small>
                            <small class="text-muted"><?php echo date('Y-m-d', strtotime($n['publish_date'])) ?></small>
                        </div>
                    </div>
                </div>
                <?php } ?>


            </div>
        </div>
        <br><br>
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <ul class="pagination justify-content-start pagination-xsm m-0">-->
<!--                    <li class="page-item disabled">-->
<!--                        <a class="page-link" href="#" aria-label="Previous">-->
<!--                            <span aria-hidden="true" class="material-icons">chevron_left</span>-->
<!--                            <span>Prev</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="page-item">-->
<!--                        <a class="page-link" href="#" aria-label="Page 1">-->
<!--                            <span>1</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="page-item">-->
<!--                        <a class="page-link" href="#" aria-label="Page 2">-->
<!--                            <span>2</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                    <li class="page-item">-->
<!--                        <a class="page-link" href="#" aria-label="Next">-->
<!--                            <span>Next</span>-->
<!--                            <span aria-hidden="true" class="material-icons">chevron_right</span>-->
<!--                        </a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->



    </div>
</div>

<?= $this->endSection() ?>
