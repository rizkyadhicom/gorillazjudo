<?= $this->extend('app') ?>



<?= $this->section('content') ?>

    <div class="page-section bg-primary border-bottom-white">
        <div class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-md-left">
            <img src="<?php echo $dataFromDB['profile_image'] ? base_url('/assets/uploads/'.$dataFromDB['profile_image']) : '/assets/images/illustration/student/128/white.svg' ?>" width="104" class="mr-md-32pt mb-32pt mb-md-0" alt="student">
            <div class="flex mb-32pt mb-md-0">
                <h2 class="text-white mb-0"><?php echo $dataFromDB['name'] ?></h2>
                <p class="lead text-white-50 d-flex align-items-center">Athlete</p>
            </div>
        </div>
    </div>

    <div class="page-section bg-primary">
        <div class="container page__container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card border-0 mb-lg-0">
                        <div class="card-body p-24pt">

                            <div class="accordion__item open">
                                <div class="accordion__menu collapse show" id="course-toc-2">
                                    <div class="accordion__menu-link">
                                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">school</i>
                                            </span>
                                        <span class="flex"><?php echo $dataFromDB['school_name'] ? $dataFromDB['school_name'] : '-' ?></span>
                                    </div>
                                    <div class="accordion__menu-link">
                                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">alternate_email</i>
                                            </span>
                                        <span class="flex"><?php echo $dataFromDB['email'] ?></span>
                                    </div>
                                    <div class="accordion__menu-link">
                                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">local_phone</i>
                                            </span>
                                        <span class="flex"><?php echo $dataFromDB['phone'] ? $dataFromDB['phone'] : '-' ?></span>
                                    </div>
                                    <div class="accordion__menu-link">
                                        <!-- <span class="material-icons icon-16pt icon--left text-muted">lock</span> -->
                                        <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                                <i class="material-icons icon-16pt">place</i>
                                            </span>
                                        <span class="flex"><?php echo $dataFromDB['address'] ? $dataFromDB['address'] : '-' ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <?php if (!empty($achievements)) { ?>
                    <div id="carouselExampleFade" class="carousel carousel-card slide mb-24pt">
                        <div class="carousel-inner">
                            <?php $i=1; foreach ($achievements as $achievement) { ?>
                            <div class="carousel-item <?php  if ($i) echo 'active'; $i=0;?>">

                                <a class="card border-0 mb-0" href="">
                                    <img src="/assets/images/achievements/flinto.png" alt="Flinto" class="card-img" style="max-height: 100%; width: initial;">
                                    <div class="fullbleed bg-primary" style="opacity: .5;"></div>
                                    <span class="card-body d-flex flex-column align-items-center justify-content-center fullbleed">
                                        <span class="row flex-nowrap">
                                            <span class="col-auto text-center d-flex flex-column justify-content-center align-items-center">
                                                <span class="text-white-60 d-block mb-24pt"><?php echo date('', strtotime($achievement['date'])) ?></span>
                                            </span>
                                        </span>
                                        <span class="row flex-nowrap">
                                            <span class="col-auto text-center d-flex flex-column justify-content-center align-items-center">
                                                <img src="/assets/images/illustration/achievement/128/white.png" width="64" alt="achievement">
                                            </span>
                                            <span class="col d-flex flex-column">
                                                <span>
                                                    <span class="card-title text-white mb-4pt d-block"><?php echo $achievement['name'] ?></span>
                                                    <span class="text-white-60"><?php echo $achievement['description'] ?></span>
                                                </span>
                                            </span>
                                        </span>
                                    </span>
                                </a>

                            </div>
                            <?php } ?>

                        </div>
                        <!-- <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
<span class="carousel-control-icon material-icons" aria-hidden="true">keyboard_arrow_left</span>
<span class="sr-only">Previous</span>
</a> -->
                        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                            <span class="carousel-control-icon material-icons" aria-hidden="true">keyboard_arrow_right</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-0 mb-lg-0">
                        <div class="card-header d-flex align-items-center border-0">
                            <div class="h2 mb-0 mr-3"><?php echo $dataChart['total'] ?? 0; ?></div>
                            <div class="flex">
                                <p class="card-title">Performance</p>
                            </div>
                            <i class="material-icons text-muted ml-2">trending_up</i>
                        </div>
                        <div class="card-body pt-0">
                            <div class="chart" style="height: 128px;">
                                <canvas id="iqChart" class="chart-canvas js-update-chart-line" data-chart-hide-axes="false" data-chart-points="true" data-chart-suffix=" score" data-chart-line-border-color="primary"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!empty($dataFromDB['about'])) {  ?>
    <div class="page-section bg-alt border-bottom-2">
        <div class="container page__container">
            <div class="row">
                <div class="col-md-6">
                    <h4>About me</h4>
                    <p><?php echo $dataFromDB['about'] ?></p>
                </div>
            </div>
        </div>
    </div>

<?php }  ?>

<?php if (!empty($dataFromDB['activities'])) {  ?>
    <div class="page-section">
        <div class="container page__container">

            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-1">
                        <div class="avatar avatar-sm mr-3">
                            <!-- <img src="assets/images/people/50/guy-2.jpg" alt="Avatar" class="avatar-img rounded-circle"> -->
                            <span class="avatar-title rounded-circle">LB</span>
                        </div>
                        <div class="flex">
                            <div class="d-flex align-items-center mb-1">
                                <strong class="card-title">Laza Bogdan</strong>
                                <small class="ml-auto text-muted">3 days ago</small>
                            </div>
                            <div>
                                <p class="measure-lead">Thanks for contributing to the release of LearnPlus - Learning Management Template <a href="">https://www.frontendmatter.com/themes/learnpl...</a> 🔥</p>
                                <p><a href="">#themeforest</a> <a href="">#EnvatoMarket</a></p>
                            </div>

                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <a href="" class="text-muted d-flex align-items-center text-decoration-0"><i class="material-icons mr-1" style="font-size: inherit;">favorite_border</i> 26</a>
                        <a href="" class="text-muted d-flex align-items-center text-decoration-0 ml-3"><i class="material-icons mr-1" style="font-size: inherit;">thumb_up</i> 123</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="d-flex mb-1">
                        <div class="avatar avatar-sm mr-3">
                            <!-- <img src="assets/images/people/50/woman-5.jpg" alt="Avatar" class="avatar-img rounded-circle"> -->
                            <span class="avatar-title rounded-circle">LB</span>
                        </div>
                        <div class="flex">
                            <div class="d-flex align-items-center mb-1">
                                <strong class="card-title">Laza Bogdan</strong>
                                <small class="ml-auto text-muted">4 days ago</small>
                            </div>
                            <div>
                                <p class="measure-lead">Checkout our new JVC camera course on <a href="">https://t.co/Wh7jE0yz4h</a> 😉
                            </div>

                            <a href="" class="card my-3 text-body text-decoration-0 measure-lead">
                                <img src="/assets/images/stories/256_rsz_phil-hearing-769014-unsplash.jpg" alt="image" class="card-img-top">
                                <span class="card-footer d-flex flex-column">
                                            <strong>Learn How To Operate a JVC Camera</strong>
                                            <span class="text-black-70">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                                            <span class="text-muted">frontendmatter.com</span>
                                        </span>
                            </a>

                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <a href="" class="text-muted d-flex align-items-center text-decoration-0"><i class="material-icons mr-1" style="font-size: inherit;">favorite_border</i> 156</a>
                        <a href="" class="text-muted d-flex align-items-center text-decoration-0 ml-3"><i class="material-icons mr-1" style="font-size: inherit;">thumb_up</i> 351</a>
                    </div>
                </div>
            </div>


            <a href="" class="btn btn-block btn-light mb-32pt">Load more ...</a>
        </div>
    </div>

<?php } ?>

<?= $this->endsection() ?>

<?= $this->section('js') ?>


<!-- Global Settings -->
<script src="/assets/js/settings.js"></script>

<!-- Moment.js -->
<script src="/assets/vendor/moment.min.js"></script>
<script src="/assets/vendor/moment-range.min.js"></script>

<!-- Chart.js -->
<script src="/assets/vendor/Chart.min.js"></script>

<!-- Charts JS -->
<script src="/assets/js/chartjs.js"></script>

<!-- Chart.js Samples -->
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });
</script>
<script>
    // 'use strict';
    //
    // window.chartColors = {
    //     red: 'rgb(255, 99, 132)',
    //     orange: 'rgb(255, 159, 64)',
    //     yellow: 'rgb(255, 205, 86)',
    //     green: 'rgb(75, 192, 192)',
    //     blue: 'rgb(54, 162, 235)',
    //     purple: 'rgb(153, 102, 255)',
    //     grey: 'rgb(201, 203, 207)'
    // };
    //
    // (function(global) {
    //     var MONTHS = [
    //         'January',
    //         'February',
    //         'March',
    //         'April',
    //         'May',
    //         'June',
    //         'July',
    //         'August',
    //         'September',
    //         'October',
    //         'November',
    //         'December'
    //     ];
    //
    //     var COLORS = [
    //         '#4dc9f6',
    //         '#f67019',
    //         '#f53794',
    //         '#537bc4',
    //         '#acc236',
    //         '#166a8f',
    //         '#00a950',
    //         '#58595b',
    //         '#8549ba'
    //     ];
    //
    //     var Samples = global.Samples || (global.Samples = {});
    //     var Color = global.Color;
    //
    //     Samples.utils = {
    //         // Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
    //         srand: function(seed) {
    //             this._seed = seed;
    //         },
    //
    //         rand: function(min, max) {
    //             var seed = this._seed;
    //             min = min === undefined ? 0 : min;
    //             max = max === undefined ? 1 : max;
    //             this._seed = (seed * 9301 + 49297) % 233280;
    //             return min + (this._seed / 233280) * (max - min);
    //         },
    //
    //         numbers: function(config) {
    //             var cfg = config || {};
    //             var min = cfg.min || 0;
    //             var max = cfg.max || 1;
    //             var from = cfg.from || [];
    //             var count = cfg.count || 8;
    //             var decimals = cfg.decimals || 8;
    //             var continuity = cfg.continuity || 1;
    //             var dfactor = Math.pow(10, decimals) || 0;
    //             var data = [];
    //             var i, value;
    //
    //             for (i = 0; i < count; ++i) {
    //                 value = (from[i] || 0) + this.rand(min, max);
    //                 if (this.rand() <= continuity) {
    //                     data.push(Math.round(dfactor * value) / dfactor);
    //                 } else {
    //                     data.push(null);
    //                 }
    //             }
    //
    //             return data;
    //         },
    //
    //         labels: function(config) {
    //             var cfg = config || {};
    //             var min = cfg.min || 0;
    //             var max = cfg.max || 100;
    //             var count = cfg.count || 8;
    //             var step = (max - min) / count;
    //             var decimals = cfg.decimals || 8;
    //             var dfactor = Math.pow(10, decimals) || 0;
    //             var prefix = cfg.prefix || '';
    //             var values = [];
    //             var i;
    //
    //             for (i = min; i < max; i += step) {
    //                 values.push(prefix + Math.round(dfactor * i) / dfactor);
    //             }
    //
    //             return values;
    //         },
    //
    //         months: function(config) {
    //             var cfg = config || {};
    //             var count = cfg.count || 12;
    //             var section = cfg.section;
    //             var values = [];
    //             var i, value;
    //
    //             for (i = 0; i < count; ++i) {
    //                 value = MONTHS[Math.ceil(i) % 12];
    //                 values.push(value.substring(0, section));
    //             }
    //
    //             return values;
    //         },
    //
    //         color: function(index) {
    //             return COLORS[index % COLORS.length];
    //         },
    //
    //         transparentize: function(color, opacity) {
    //             var alpha = opacity === undefined ? 0.5 : 1 - opacity;
    //             return Color(color).alpha(alpha).rgbString();
    //         }
    //     };
    //
    //     // DEPRECATED
    //     window.randomScalingFactor = function() {
    //         return Math.round(Samples.utils.rand(-100, 100));
    //     };
    //
    //     // INITIALIZATION
    //
    //     Samples.utils.srand(Date.now());
    //
    //     // Google Analytics
    //     /* eslint-disable */
    //     if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
    //         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    //             (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    //             m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    //         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    //         ga('create', 'UA-28909194-3', 'auto');
    //         ga('send', 'pageview');
    //     }
    //     /* eslint-enable */
    //
    // }(this));
    //
    // var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    // var config = {
    //     type: 'line',
    //     data: {
    //         labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //         datasets: [{
    //             label: 'My First dataset',
    //             backgroundColor: window.chartColors.red,
    //             borderColor: window.chartColors.red,
    //             data: [
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor()
    //             ],
    //             fill: false,
    //         }, {
    //             label: 'My Second dataset',
    //             fill: false,
    //             backgroundColor: window.chartColors.blue,
    //             borderColor: window.chartColors.blue,
    //             data: [
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor(),
    //                 randomScalingFactor()
    //             ],
    //         }]
    //     },
    //     options: {
    //         responsive: true,
    //         title: {
    //             display: true,
    //             text: 'Chart.js Line Chart'
    //         },
    //         tooltips: {
    //             mode: 'index',
    //             intersect: false,
    //         },
    //         hover: {
    //             mode: 'nearest',
    //             intersect: true
    //         },
    //         scales: {
    //             xAxes: [{
    //                 display: true,
    //                 scaleLabel: {
    //                     display: true,
    //                     labelString: 'Month'
    //                 }
    //             }],
    //             yAxes: [{
    //                 display: true,
    //                 scaleLabel: {
    //                     display: true,
    //                     labelString: 'Value'
    //                 }
    //             }]
    //         }
    //     }
    // };
    //
    // window.onload = function() {
    //     var ctx = document.getElementById('iqChart').getContext('2d');
    //     window.myLine = new Chart(ctx, config);
    // };

    // document.getElementById('randomizeData').addEventListener('click', function() {
    //     config.data.datasets.forEach(function(dataset) {
    //         dataset.data = dataset.data.map(function() {
    //             return randomScalingFactor();
    //         });
    //
    //     });
    //
    //     window.myLine.update();
    // });
    //
    // var colorNames = Object.keys(window.chartColors);
    // document.getElementById('addDataset').addEventListener('click', function() {
    //     var colorName = colorNames[config.data.datasets.length % colorNames.length];
    //     var newColor = window.chartColors[colorName];
    //     var newDataset = {
    //         label: 'Dataset ' + config.data.datasets.length,
    //         backgroundColor: newColor,
    //         borderColor: newColor,
    //         data: [],
    //         fill: false
    //     };
    //
    //     for (var index = 0; index < config.data.labels.length; ++index) {
    //         newDataset.data.push(randomScalingFactor());
    //     }
    //
    //     config.data.datasets.push(newDataset);
    //     window.myLine.update();
    // });
    //
    // document.getElementById('addData').addEventListener('click', function() {
    //     if (config.data.datasets.length > 0) {
    //         var month = MONTHS[config.data.labels.length % MONTHS.length];
    //         config.data.labels.push(month);
    //
    //         config.data.datasets.forEach(function(dataset) {
    //             dataset.data.push(randomScalingFactor());
    //         });
    //
    //         window.myLine.update();
    //     }
    // });
    //
    // document.getElementById('removeDataset').addEventListener('click', function() {
    //     config.data.datasets.splice(0, 1);
    //     window.myLine.update();
    // });
    //
    // document.getElementById('removeData').addEventListener('click', function() {
    //     config.data.labels.splice(-1, 1); // remove the label first
    //
    //     config.data.datasets.forEach(function(dataset) {
    //         dataset.data.pop();
    //     });
    //
    //     window.myLine.update();
    // });
</script>
<script>
    !function (e) {
        var t = {};

        function r(n) {
            if (t[n]) return t[n].exports;
            var o = t[n] = {i: n, l: !1, exports: {}};
            return e[n].call(o.exports, o, o.exports, r), o.l = !0, o.exports
        }

        r.m = e, r.c = t, r.d = function (e, t, n) {
            r.o(e, t) || Object.defineProperty(e, t, {enumerable: !0, get: n})
        }, r.r = function (e) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: "Module"}), Object.defineProperty(e, "__esModule", {value: !0})
        }, r.t = function (e, t) {
            if (1 & t && (e = r(e)), 8 & t) return e;
            if (4 & t && "object" == typeof e && e && e.__esModule) return e;
            var n = Object.create(null);
            if (r.r(n), Object.defineProperty(n, "default", {
                enumerable: !0,
                value: e
            }), 2 & t && "string" != typeof e) for (var o in e) r.d(n, o, function (t) {
                return e[t]
            }.bind(null, o));
            return n
        }, r.n = function (e) {
            var t = e && e.__esModule ? function () {
                return e.default
            } : function () {
                return e
            };
            return r.d(t, "a", t), t
        }, r.o = function (e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }, r.p = "/", r(r.s = 399)
    }({
        399: function (e, t, r) {
            e.exports = r(400)
        }, 400: function (e, t) {
            !function () {
                "use strict";
                // var e = [], t = moment().subtract(6, "days").format("YYYY-MM-DD"), r = moment().format("YYYY-MM-DD");
                // moment.range(t, r).by("days", (function (t) {
                //     e.push({y: Math.floor(200 * Math.random()) + 15, x: t.toDate()})
                // }));
                var e = <?php echo json_encode($dataChart['data'], true) ?>;
                console.log(e);
                !function (t) {
                    var r = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "line",
                        n = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                    n = Chart.helpers.merge({
                        scales: {
                            yAxes: [{ticks: {maxTicksLimit: 4}}],
                            xAxes: [{
                                type: 'category',
                                labels: <?php echo json_encode($dataChart['label'], true) ?>
                            }]
                        }
                    }, n);
                    var o = {datasets: [{label: ["Performance"], data: e}]};
                    Charts.create(t, r, n, o)
                }("#iqChart")
            }()
        }
    });


</script>

<?= $this->endsection() ?>
