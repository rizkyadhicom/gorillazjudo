<?= $this->extend('app') ?>

<?= $this->section('content') ?>

<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="d-flex flex-column flex-md-row align-items-center align-items-md-start flex mb-16pt mb-lg-0 text-center text-md-left">
                <div class="avatar overlay overlay--primary mb-16pt mb-md-0 mr-md-16pt">
                    <img src="/assets/uploads/<?php echo $news['cover_image'] ?>" class="avatar-img rounded" alt="lesson">
                    <div class="overlay__content"></div>
                </div>
                <div class="flex">
                    <h1 class="h2 measure-lead-max mb-16pt"><?php echo $news['title'] ?></h1>
                    <div class="d-flex align-items-center">
                        <div class="mr-16pt">
                            <a href="#" class="card-title"><?php echo $news['author'] ?></a>
                            <div class="d-flex align-items-center">
                                <small class="text-50 mr-2"><?php echo date('Y-m-d', strtotime($news['publish_date'])) ?></small>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>

<div class="page-section border-bottom-2">
    <div class="container page__container">

        <div class="row">
            <div class="col-lg-2">

            </div>
            <div class="col-lg-8">

                <?php echo $news['content'] ?>

            </div>
            <div class="col-lg-2">

            </div>
        </div>

    </div>
</div>
<?= $this->endSection() ?>
