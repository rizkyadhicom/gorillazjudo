<<?= $this->extend('app') ?>

<?= $this->section('content') ?>



<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="flex d-flex flex-column align-items-center align-items-lg-start mb-16pt mb-lg-0 text-center text-lg-left">
                <h1 class="h2 mb-8pt"><?php echo $program['name'] ?></h1>
            </div>
        </div>

    </div>
</div>


<div class="page-section">
    <div class="container page__container">

        <div class="row">
            <div class="col-lg-8">





                <div class="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt" style="white-space: nowrap;">
                    <small class="flex text-muted text-headings text-uppercase mr-3 mb-2 mb-sm-0">Displaying training results</small>
                </div>


                <div class="row card-group-row">
                    <?php foreach ($training as $train) { ?>
                    <div class="card-group-row__col col-md-6">

                        <div class="card card-group-row__card card-sm">
                            <div class="card-body d-flex align-items-center">
                                <a href="" class="avatar overlay overlay--primary avatar-4by3 mr-12pt">
                                    <img src="/assets/images/paths/typescript_200x168.png" alt="Introduction to TypeScript" class="avatar-img rounded">
                                    <span class="overlay__content"></span>
                                </a>
                                <div class="flex mr-12pt">
                                    <a class="card-title" href=""><?php echo $train['user']['name'] ?></a>
                                    <div class="card-subtitle text-50"></div>
                                </div>
                                <div class="d-flex flex-column align-items-center">
                                    <?php if ($train['score'] > 0 && $train['status'] == 'COMPLETED') { ?>
                                        <span class="lead text-headings lh-1"><?php echo $train['score'] ?></span>
                                        <small class="text-50 text-uppercase text-headings">Score</small>
                                    <?php }else{ ?>
                                        <span class="lead text-headings lh-1">Need Review</span>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="d-flex align-items-center">
                                    <div class="flex mr-2">
                                        <?php if ($train['score'] > 0 && $train['status'] == 'COMPLETED') { ?>
                                            <a href="<?php echo base_url('/training/review/'.$train['program_id'].'/'.$train['user_id']) ?>" class="btn btn-light btn-sm">
                                                <i class="material-icons icon--left">playlist_add_check</i> Review
                                            </a>
                                        <?php }else{ ?>
                                            <a href="<?php echo base_url('/training/review/'.$train['program_id'].'/'.$train['user_id']) ?>" class="btn btn-light btn-sm">
                                                <i class="material-icons icon--left">playlist_add_check</i> Review
                                            </a>
                                        <?php } ?>
                                        <span class="lead text-headings lh-1" style="float:right"><?php echo $train['date'] ?></span>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <?php } ?>

                </div>

            </div>
        </div>

    </div>
</div>

<?= $this->endSection() ?>