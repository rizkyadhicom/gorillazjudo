<?= $this->extend('app') ?>


<?= $this->section('css') ?>

<?= $this->endSection() ?>

<?= $this->section('js') ?>


<?= $this->endSection() ?>


<?= $this->section('content') ?>



<div class="page-section bg-alt border-bottom-2">
    <div class="container page__container">

        <div class="d-flex flex-column flex-lg-row align-items-center">
            <div class="d-flex flex-column flex-md-row align-items-center flex mb-16pt mb-lg-0 text-center text-md-left">
                <div class="avatar mb-16pt mb-md-0 mr-md-16pt">
                    <img src="/assets/uploads/<?php echo $program['thumbnail'] ?>" class="avatar-img rounded" alt="lesson">
                </div>
                <div class="flex">
                    <h1 class="h2 m-0"><?php echo $program['name'] ?> </h1>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="page-section border-bottom-2">
    <div class="container page__container">

        <div class="row">
            <div class="col-lg-8">

                <div class="js-player card bg-primary embed-responsive embed-responsive-16by9 mb-24pt">
                    <div class="player embed-responsive-item">
                        <div class="player__content">
                            <div class="player__image" style="--player-image: url(/assets/images/illustration/player.svg)"></div>
                            <a href="" class="player__play bg-primary">
                                <span class="material-icons">play_arrow</span>
                            </a>
                        </div>
                        <div class="player__embed d-none">
                            <video width="320" height="240" controls>
                                <source src="<?php echo base_url('/assets/uploads/'.$currentVideo['video_url']) ?>" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
<!--                            <iframe class="embed-responsive-item" src="" allowfullscreen=""></iframe>-->
                        </div>
                    </div>
                </div>

                <div class="mb-24pt">
                    <span class="chip chip-outline-secondary d-inline-flex align-items-center">
                        <i class="material-icons icon--left">video_label</i>
                        <?php echo $currentVideo['name'] ?>
                    </span>
                    <span class="chip chip-outline-secondary d-inline-flex align-items-center">
                            <i class="material-icons icon--left">assessment</i>
                            Beginner
                        </span>
                </div>
                <div class="lead measure-lead text-70 mb-24pt">
                    <?php echo $program['description'] ?>
                </div>

                <div class="page-separator">
                    <div class="page-separator__text">Syllabus</div>
                </div>



                <div class="accordion js-accordion accordion--boxed " id="parent">

                    <div class="accordion__item open">
                        <span class="accordion__toggle" >
                            <span class="flex">Download Syllabus</span>
                        </span>
                        <div class="accordion__menu collapse show" id="course-toc-2">
                            <?php foreach ($program['syllabus'] as $syllabus){ ?>
                            <div class="accordion__menu-link">
                                <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                    <i class="material-icons icon-16pt">attach_file</i>
                                </span>
                                <a class="flex" href="<?php echo base_url('/assets/uploads/'.$syllabus['file_url']) ?>">
                                    <?php echo $syllabus['name'] ?>
                                    <span class="text-muted pull-right">Click to Download</span>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <br>
                <?php

                $this->session = \Config\Services::session();
                $is_login = $this->session->get('is_login');
                if ($is_login) { ?>

                <div class="page-separator">
                    <div class="page-separator__text">Input Your Training Result</div>
                </div>

                <div class="accordion js-accordion accordion--boxed " id="parent">

                    <div class="accordion__item open">
                        <span class="accordion__toggle" >
                            <span class="flex">Result</span>
                        </span>
                        <div class="accordion__menu collapse show" id="course-toc-2">
                            <div class="accordion__menu-link">
                                <span class="icon-holder icon-holder--small icon-holder--dark rounded-circle d-inline-flex icon--left">
                                    <i class="material-icons icon-16pt">star</i>
                                </span>
                                <a class="flex" href="<?php echo base_url('/training/program/'.$program['id']) ?>">
                                    No Result Found
                                    <span class="text-muted pull-right">Click Here to Upload Your Training Result</span>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>
            <div class="col-lg-4">

                <div class="page-separator">
                    <div class="page-separator__text">Videos</div>
                </div>
                <?php foreach ($program['videos'] as $video) { ?>
                    <a href="<?php echo base_url('program/detail/'.$program['id'].'?video_id='.$video['id']); ?>" class="d-flex flex-nowrap mb-24pt">
                        <span class="mr-16pt">
                            <img src="/assets/images/paths/mailchimp_200x168.png" width="80" alt="Angular" class="rounded">
                        </span>
                        <span class="flex d-flex flex-column align-items-start" style="justify-content: center;">
                            <span class="card-title"><?php echo $video['name'] ?></span>
                        </span>
                    </a>
                <?php } ?>

                <div class="page-separator">
                    <div class="page-separator__text">Pelatih</div>
                </div>

                <div class="media align-items-center mb-16pt">
                                <span class="media-left mr-16pt">
                                    <img src="<?php echo @$program['instructor']['profile_image'] ? '/assets/uploads/'.$program['instructor']['profile_image'] : '/assets/images/people/110/guy-3.jpg' ?>" width="40" alt="avatar" class="rounded-circle">
                                </span>
                    <div class="media-body">
                        <a class="card-title m-0" href="<?php echo base_url('/account/profile/'.@$program['instructor']['id']) ?>"><?php echo $program['instructor']['name'] ?></a>
                        <p class="text-50 lh-1 mb-0">Instructor</p>
                    </div>
                </div>
                <p class="text-70"><?php echo @$program['instructor']['about'] ?></p>

                <a href="<?php echo base_url('/account/profile/'.@$program['instructor']['id']) ?>" class="btn btn-white mb-24pt">See Profile</a>

            </div>
        </div>

    </div>
</div>



<?= $this->endSection() ?>
