<?php
    $this->session = \Config\Services::session();

use App\Models\NewsModel; ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <link href="https://fonts.googleapis.com/css?family=Lato:400,700%7CRoboto:400,500%7CExo+2:600&display=swap"
          rel="stylesheet">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="/assets/vendor/perfect-scrollbar.css" rel="stylesheet">

    <!-- Fix Footer CSS -->
    <link type="text/css" href="/assets/vendor/fix-footer.css" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="/assets/css/material-icons.css" rel="stylesheet">


    <!-- Font Awesome Icons -->
    <link type="text/css" href="/assets/css/fontawesome.css" rel="stylesheet">


    <!-- Preloader -->
    <link type="text/css" href="/assets/css/preloader.css" rel="stylesheet">


    <!-- App CSS -->
    <link type="text/css" href="/assets/css/app.css" rel="stylesheet">

    <?= $this->renderSection('css') ?>

</head>


<body class="layout-sticky-subnav layout-learnly ">

<div class="preloader">
    <div class="sk-double-bounce">
        <div class="sk-child sk-double-bounce1"></div>
        <div class="sk-child sk-double-bounce2"></div>
    </div>
</div>

<!-- Header Layout -->
<div class="mdk-header-layout js-mdk-header-layout">

    <!-- Header -->

    <div id="header" class="mdk-header js-mdk-header mb-0" data-fixed data-effects="waterfall">
        <div class="mdk-header__content">


            <div class="navbar navbar-expand navbar-light bg-white navbar-shadow" id="default-navbar" data-primary>
                <div class="container page__container">

                    <!-- Navbar Brand -->
                    <a href="<?php echo base_url(''); ?>" class="navbar-brand mr-16pt">

                            <span class="avatar avatar-lg navbar-brand-icon mr-0 mr-lg-8pt" style="padding: 4px;width: 130px !important;">

                                <span class="rounded"><img src="/assets/logo.jpeg" alt="logo" class="img-fluid"
                                                           style="border-radius: 5px;width: 45px !important;width: 130px !important;"/></span>

                            </span>

                        <span class="d-none d-lg-block">Gorillaz Judo</span>
                    </a>

                    <ul class="nav navbar-nav d-none d-sm-flex flex justify-content-start ml-8pt">
                        <li class="nav-item active">
                            <a href="<?php echo base_url(''); ?>" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('/program'); ?>" class="nav-link">Go Kyo</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('/news'); ?>" class="nav-link">News & Event</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('/contact'); ?>" class="nav-link">Contact Us</a>
                        </li>
                    </ul>

                    <form class="search-form form-control-rounded navbar-search d-none d-lg-flex mr-16pt"
                          action="<?php echo base_url('/program')?>" style="max-width: 230px">
                        <button class="btn" type="submit"><i class="material-icons">search</i></button>
                        <input type="text" name="search" class="form-control" placeholder="Search ...">
                    </form>


                    <?php

                    $this->session = \Config\Services::session();
                    $is_login = $this->session->get('is_login');
                    if (!$is_login) {
                        ?>
                        <ul class="nav navbar-nav ml-auto mr-0">
                            <li class="nav-item">
                                <a href="<?php echo base_url('/login'); ?>" class="btn btn-outline-secondary"><i
                                            class="material-icons icon--left">cloud</i> Login</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('/register'); ?>" class="btn btn-outline-secondary"><i
                                            class="material-icons icon--left">cloud</i> Register</a>
                            </li>
                        </ul>
                    <?php } else { ?>
                        <div class="nav navbar-nav ml-auto mr-0 flex-nowrap d-flex">
                            <?php

                            function time_elapsed_string($datetime, $full = false) {
                                $now = new DateTime;
                                $ago = new DateTime($datetime);
                                $diff = $now->diff($ago);

                                $diff->w = floor($diff->d / 7);
                                $diff->d -= $diff->w * 7;

                                $string = array(
                                    'y' => 'year',
                                    'm' => 'month',
                                    'w' => 'week',
                                    'd' => 'day',
                                    'h' => 'hour',
                                    'i' => 'minute',
                                    's' => 'second',
                                );
                                foreach ($string as $k => &$v) {
                                    if ($diff->$k) {
                                        $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                                    } else {
                                        unset($string[$k]);
                                    }
                                }

                                if (!$full) $string = array_slice($string, 0, 1);
                                return $string ? implode(', ', $string) . ' ago' : 'just now';
                            }

                            $userId = $this->session->get('user_id');
                            $countNotif = 0;
                            $listNotif = [];
                            if (!empty($userId)){
                                $notifModel = new \App\Models\UserActivityModel();
                                $listNotif = $notifModel->getDataByWhere(['user_id'=>$userId]);
                                $countNotif = $notifModel->countDataByWhere(['user_id'=>$userId]);
//                                $listNotif = $notifModel->getDataByWhere();
//                                $countNotif = $notifModel->countDataByWhere();
                            }
                            ?>
                            <!-- Notifications dropdown -->
                            <div class="nav-item ml-16pt dropdown dropdown-notifications dropdown-xs-down-full"
                                 data-toggle="tooltip" data-title="Notifications" data-placement="bottom"
                                 data-boundary="window">
                                <button class="nav-link btn-flush dropdown-toggle" type="button" data-toggle="dropdown"
                                        data-caret="false">
                                    <i class="material-icons">notifications_none</i>
                                    <?php if ($countNotif > 0) { ?><span class="badge badge-notifications badge-accent"><?php echo $countNotif ?></span><?php } ?>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div data-perfect-scrollbar class="position-relative">
                                        <div class="dropdown-header"><strong>Notifications</strong></div>
                                        <div class="list-group list-group-flush mb-0">
                                            <?php

                                            foreach ($listNotif as $notif){
                                            ?>
                                                <a href="<?php echo $notif['url'] ?? 'javascript:void(0);'; ?> "
                                                   class="list-group-item list-group-item-action unread">
                                                    <span class="d-flex align-items-center mb-1">
                                                        <small class="text-black-50"><?php echo time_elapsed_string($notif['datetime']); ?></small>

                                                        <span class="ml-auto unread-indicator bg-accent"></span>

                                                    </span>
                                                    <span class="d-flex">
                                                        <span class="avatar avatar-xs mr-2">
                                                            <span class="avatar-title rounded-circle bg-light">
                                                                <i class="material-icons font-size-16pt text-accent">account_circle</i>
                                                            </span>
                                                        </span>
                                                        <span class="flex d-flex flex-column">

                                                            <span class="text-black-70"><?php echo $notif['description']; ?></span>
                                                        </span>
                                                    </span>
                                                </a>
                                            <?php
                                            }
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // END Notifications dropdown -->


                            <div class="nav-item dropdown">
                                <a href="#" class="nav-link d-flex align-items-center dropdown-toggle"
                                   data-toggle="dropdown" data-caret="false">

                                    <span class="avatar avatar-sm mr-8pt2">

                                        <span class="avatar-title rounded-circle bg-primary"><i class="material-icons">account_box</i></span>

                                    </span>

                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-header"><strong>Account</strong></div>
                                    <a class="dropdown-item" href="<?php echo base_url('/account') ?>">Profile Account</a>
                                    <a class="dropdown-item" href="<?php echo base_url('/account/edit') ?>">Edit Account</a>
                                    <a class="dropdown-item" href="<?php echo base_url('/logout') ?>">Logout</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>

    <!-- // END Header -->

    <!-- Header Layout Content -->
    <div class="mdk-header-layout__content page-content ">


        <?= $this->renderSection('content') ?>


    </div>
    <!-- // END Header Layout Content -->


    <div class="js-fix-footer2 bg-dark border-top-2">
        <div class="container page__container page-section d-flex flex-column">
            <div class="row text-white">
                <div class="col-md-3">
                    <h4 class="text-white">Contact Us</h4>
                    <p>
                        0857-1490-6906 <br>
                        Perum Unitex, Jl.Melati XI, <br>
                        Blok D-5, RT.05/RW.06, <br>
                        Sindangrasa, <br>
                        Kec. Bogor Tim., <br>
                        Kota Bogor
                    </p>
                </div>
                <div class="col-md-3">
                    <h4 class="text-white">Latest News</h4>
                    <?php

                    $this->newsModel = new NewsModel();
                    $footerNews = $this->newsModel->getDataByWhere([],0,5);
                    foreach ($footerNews as $n) { ?>
                        <div class="mb-8pt d-flex align-items-center">
                            <a href="learnly-blog-post.html" class="avatar avatar-sm overlay overlay--primary mr-12pt">
                                <img src="/assets/uploads/<?php echo $n['cover_image']?>" alt="/assets/uploads/<?php echo $n['cover_image']?>" class="avatar-img rounded">
                                <span class="overlay__content"></span>
                            </a>
                            <div class="flex">
                                <a class="card-title mb-4pt" href="<?php echo base_url('/news/detail/'.$n['id']) ?>"><?php echo $n['title'] ?></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-3">
                    <h4 class="text-white">Partners</h4>
                    <img src="/assets/kerjasama.jpeg" class="rounded mb-8pt" width="150"><br>
                    <img src="/assets/kerjasama2.jpeg" class="rounded mb-8pt" width="150">
                </div>
                <div class="col-md-3">
                    <h4 class="text-white">Location</h4>
                    <iframe
                            width="300"
                            height="300"
                            frameborder="0" style="border:0"
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyD9RUNa-NOfKGJsQeh8YtvQHxLDrprVNXU
    &q=Gorillaz+Judo+Bogor" allowfullscreen>
                    </iframe>
                </div>
            </div>

            <p class="text-white-50 small mt-n1 mb-0">Copyright 2019 &copy; All rights reserved.</p>
        </div>
    </div>


</div>
<!-- // END Header Layout -->

<!-- jQuery -->
<script src="/assets/vendor/jquery.min.js"></script>

<!-- Bootstrap -->
<script src="/assets/vendor/popper.min.js"></script>
<script src="/assets/vendor/bootstrap.min.js"></script>

<!-- Perfect Scrollbar -->
<script src="/assets/vendor/perfect-scrollbar.min.js"></script>

<!-- DOM Factory -->
<script src="/assets/vendor/dom-factory.js"></script>

<!-- MDK -->
<script src="/assets/vendor/material-design-kit.js"></script>

<!-- Fix Footer -->
<script src="/assets/vendor/fix-footer.js"></script>

<!-- App JS -->
<script src="/assets/js/app.js"></script>


<?= $this->renderSection('js') ?>

</body>

</html>
