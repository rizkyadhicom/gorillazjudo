<?= $this->extend('cms_app') ?>

<?= $this->section('content') ?>



<div class="pt-32pt">
    <div class="container page__container d-flex flex-column flex-md-row align-items-center text-center text-sm-left">
        <div class="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">

            <div class="mb-24pt mb-sm-0 mr-sm-24pt">
                <h2 class="mb-0">Dashboard</h2>

                <ol class="breadcrumb p-0 m-0">
                    <li class="breadcrumb-item"><a href="">Dashboard</a></li>

                    <li class="breadcrumb-item active">

                        Manage News

                    </li>

                </ol>

            </div>
        </div>

    </div>
</div>


<div class="page-section border-bottom-2">
    <div class="container page__container">

        <div class="page-separator">
            <div class="page-separator__text">Basic Information</div>
        </div>

        <div class="card mb-lg-32pt">


            <div class="container-fluid page__container page-section">

                <form id="formId" action="<?php echo base_url('admin/savenews'); ?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo @$dataFromDB['id'] ? @$dataFromDB['id'] : 0 ?>">
                    <input type="hidden" name="type" value="">
                    <input type="hidden" name="slug" value="">
                    <input type="hidden" name="cover_image" value="">
                    <div class="form-group">
                        <label class="form-label">News title</label>
                        <input type="text" name="title" class="form-control" value="<?php echo @$dataFromDB['title'] ?>" placeholder="Masukkan Judul Berita">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Publish Date</label>
                        <input type="date" name="publish_date" class="form-control" value="<?php echo date('Y-m-d', strtotime(@$dataFromDB['publish_date'])) ?>">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Author</label>
                        <input type="text" name="author" class="form-control" value="<?php echo @$dataFromDB['author'] ?>" placeholder="Masukkan Nama Author">
                    </div>
                    <div class="form-group mb-32pt">
                        <label class="form-label">Description</label>
                        <input type="hidden" id="description" name="content" value="<?php echo @$dataFromDB['content'] ?>">
                        <!--                             <textarea id="editor" class="form-control" name="description" rows="3" placeholder="Course description">--><?php //echo @$dataFromDB['description'] ?><!--</textarea>-->
                        <div style="height: 150px;" data-toggle="quill" data-quill-placeholder="Description">
                            <?php echo @$dataFromDB['content'] ?>
                        </div>
                    </div>
                    <div class="card">
                        <div class="embed-responsive embed-responsive-16by9">
                            <img class="embed-responsive-item" src="<?php echo @$dataFromDB['cover_image'] ? base_url('/assets/uploads/'.@$dataFromDB['cover_image']) : '' ?>" allowfullscreen=""></img>
                        </div>
                        <div class="card-body">
                            <label class="form-label">Cover Image</label>
                            <div class="custom-file">
                                <input type="file" name="thumbnail" id="file" class="form-control">
                            </div>
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Save changes</button>

                    <?php if (!empty($dataFromDB['id'])){ ?>
                        <a href="<?php echo base_url('/admin/removenews/'.$dataFromDB['id'])?>" class="btn btn-danger">Delete</a>
                    <?php } ?>
                </form>


            </div>

        </div>

    </div>
</div>



<?= $this->endSection() ?>

<?= $this->section('css') ?>

<link type="text/css" href="/assets/css/quill.css" rel="stylesheet">

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<!-- Quill -->
<script src="/assets/vendor/quill.min.js"></script>
<script src="/assets/js/quill.js"></script>
<!-- Select2 -->
<script src="/assets/vendor/select2/select2.min.js"></script>
<script src="/assets/js/select2.js"></script>

<script>
    var quill = new Quill('#editor', {
        theme: 'snow'
    });
    $(document).ready(function(){
        $("#formId").on("submit", function () {
            $('#description').val($('.ql-editor').html());
        });
    });

</script>
<?= $this->endSection() ?>



