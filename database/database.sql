-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.21-0ubuntu0.20.04.4 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table gorillaz.news
DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `type` varchar(50) DEFAULT NULL,
  `publish_date` date DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `slug` varchar(100) DEFAULT NULL,
  `cover_image` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.news: ~2 rows (approximately)
DELETE FROM `news`;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `title`, `content`, `type`, `publish_date`, `author`, `slug`, `cover_image`) VALUES
	(1, 'asdas', '<p>asdasdasd</p>', '', '2020-07-17', 'asd', '', 'native_2.jpg'),
	(2, 'asdasda', '<p>asdasdasdasd</p>', '', '2020-07-17', 'asdasd', '', '');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table gorillaz.program
DROP TABLE IF EXISTS `program`;
CREATE TABLE IF NOT EXISTS `program` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `description` text,
  `user_id` int DEFAULT NULL,
  `gokyo` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.program: ~4 rows (approximately)
DELETE FROM `program`;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;
INSERT INTO `program` (`id`, `name`, `thumbnail`, `description`, `user_id`, `gokyo`) VALUES
	(14, 'De ashi harai', 'De ashi harai (thumbnail).jpeg', '<p>De ashi harai adalah teknik sapuan kaki.&nbsp;</p><p>Dalam hal ini kaki yang akan disapu adalah kaki yang berada di depan&nbsp;</p><p>baik waktu maju atau mundur.</p><p>Posisi kaki yang akan disapu adalah pada saat kaki akan melangkah&nbsp;</p><p>meninggalkan matras atau pada saat akan menginjak matras&nbsp;</p><p>atau pada saat kaki akan menginjak matras</p><p>Dengan kata lain, pada saat tumpuan kaki akan pindah ke kaki yang di depan&nbsp;</p><p>atau yang di belakang.</p><p>kusushi yang dipergunakan adalah ke muka atau ke belakang.</p><p><br></p>', 13, 'Go Kyo 1'),
	(15, 'O uchi gari', 'O uchi gari (thumbnail).jpeg', '<p>Teknik ini menyabit kaki dari arah dalam. kusushi yang dipakai&nbsp;</p><p>adalah dengan mendorongnya ke belakang, bersamaan dengan gerakan menyabit</p><p>kaki uke dari dalam desertai dorongan tangan Tori, sehingga Uke jatuh ke belakang.</p>', 13, 'Go Kyo 1'),
	(16, 'Ippon seoi nage', 'Ippon seoi nage (thumbnail).jpeg', '<p>Teknik ini adalah teknik bantingan tangan, siku Tori menjepit ketiak uke,</p><p>kemudian badan tori beputar 180 derajat,&nbsp;</p><p>sehingga posisinya sejajar dengan badan uke.</p><p>Dengan posisi lutut yang sedikit di tekuk, kemudian badan Uke di hentak keatas,&nbsp;</p><p>pinggul membantu hentakan, sehingga Uke terlempar ke depan.&nbsp;</p>', 13, 'Go Kyo 1');
/*!40000 ALTER TABLE `program` ENABLE KEYS */;

-- Dumping structure for table gorillaz.program_syllabus
DROP TABLE IF EXISTS `program_syllabus`;
CREATE TABLE IF NOT EXISTS `program_syllabus` (
  `id` int NOT NULL AUTO_INCREMENT,
  `program_id` int DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `file_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table gorillaz.program_syllabus: ~5 rows (approximately)
DELETE FROM `program_syllabus`;
/*!40000 ALTER TABLE `program_syllabus` DISABLE KEYS */;
INSERT INTO `program_syllabus` (`id`, `program_id`, `name`, `file_url`) VALUES
	(1, 5, 'Tampak Depan', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(2, 5, 'Cideo Dari Samping', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(3, 5, 'Video Dari Belakang', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(4, 6, 'syllab', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(5, 7, 'file nya', 'Meet - bsy-ahaz-xuq - Google Chrome 2020-05-19 16-52-57_5.mp4'),
	(6, 14, 'de ashi harai', 'de ashi harai.pdf'),
	(7, 15, 'O uchi gari', 'o uchi gari.pdf'),
	(8, 16, 'Ippon seoi nage', 'ippon seoi.pdf');
/*!40000 ALTER TABLE `program_syllabus` ENABLE KEYS */;

-- Dumping structure for table gorillaz.program_training
DROP TABLE IF EXISTS `program_training`;
CREATE TABLE IF NOT EXISTS `program_training` (
  `id` int NOT NULL AUTO_INCREMENT,
  `program_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `score` int DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- Dumping data for table gorillaz.program_training: ~4 rows (approximately)
DELETE FROM `program_training`;
/*!40000 ALTER TABLE `program_training` DISABLE KEYS */;
INSERT INTO `program_training` (`id`, `program_id`, `user_id`, `score`, `status`, `video_url`, `date`) VALUES
	(6, 5, 3, 0, 'REVIEW', 'asd_5.jpg', '2020-07-23'),
	(7, 5, 3, 0, 'REVIEW', 'Meet - bsy-ahaz-xuq - Google Chrome 2020-05-19 16-52-57.mp4', '2020-07-28'),
	(8, 5, 10, 0, 'REVIEW', 'Meet - bsy-ahaz-xuq - Google Chrome 2020-05-19 16-52-57_1.mp4', '2020-07-29'),
	(9, 7, 3, 0, 'REVIEW', 'Meet - bsy-ahaz-xuq - Google Chrome 2020-05-19 16-52-57_6.mp4', '2020-07-29');
/*!40000 ALTER TABLE `program_training` ENABLE KEYS */;

-- Dumping structure for table gorillaz.program_training_comment
DROP TABLE IF EXISTS `program_training_comment`;
CREATE TABLE IF NOT EXISTS `program_training_comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `program_training_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `comment` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.program_training_comment: ~6 rows (approximately)
DELETE FROM `program_training_comment`;
/*!40000 ALTER TABLE `program_training_comment` DISABLE KEYS */;
INSERT INTO `program_training_comment` (`id`, `program_training_id`, `user_id`, `comment`, `date`) VALUES
	(1, 6, 10, 'tes', '2020-08-02 00:00:00'),
	(2, 6, 10, 'tess', '2020-08-02 00:00:00'),
	(3, 7, 10, 'aa', '2020-08-02 00:00:00'),
	(4, 8, 10, 'a', '2020-08-02 00:00:00'),
	(5, 6, 3, 'tes', '2020-08-02 00:00:00'),
	(6, 7, 3, 'tess', '2020-08-02 00:00:00');
/*!40000 ALTER TABLE `program_training_comment` ENABLE KEYS */;

-- Dumping structure for table gorillaz.program_videos
DROP TABLE IF EXISTS `program_videos`;
CREATE TABLE IF NOT EXISTS `program_videos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `program_id` int DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.program_videos: ~9 rows (approximately)
DELETE FROM `program_videos`;
/*!40000 ALTER TABLE `program_videos` DISABLE KEYS */;
INSERT INTO `program_videos` (`id`, `program_id`, `name`, `video_url`) VALUES
	(1, 5, 'Tampak Depan', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(2, 5, 'Cideo Dari Samping', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(3, 5, 'Video Dari Belakang', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(4, 3, 'De ashi harai', 'trim.E59A9B0D-1595-49F5-BD4D-A136FB83274E.MOV'),
	(5, 1, 'De ashi harai', 'trim.1708C592-EF49-46E7-8C66-2045A0368953.MOV'),
	(6, 3, 'Gerakan', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(7, 5, 'Go Kyo', 'New Tab - Google Chrome 2020-05-19 16-40-06.mp4'),
	(8, 2, 'De ashi harai', 'Video-20200711-102931-391.mp4'),
	(9, 3, 'O uchi gari ', 'Video-20200711-105640-076.mp4'),
	(10, 4, 'Seoi nage', 'Video-20200711-114024-810.mp4'),
	(11, 13, 'Jatuhan belakang', 'trim.185FDD85-0C25-4F0D-86EC-6871D4F5C222.MOV'),
	(12, 14, 'Inti', 'De ashi harai video inti.mp4'),
	(13, 14, 'Posisi kaki', 'Posisi kaki De ashi harai.mp4'),
	(14, 14, 'Posisi Tangan', 'Posisi tangan De ashi harai.mp4'),
	(17, 14, 'Shadow Uchikomi', 'Shadow uchikomi de ashi harai.mp4'),
	(18, 15, 'Inti', 'O uchi gari (video inti).mp4'),
	(19, 15, 'Posisi kaki', 'Posisi kaki O uchi gari.mp4'),
	(20, 15, 'Posisi Tangan', 'Posisi tangan O uchi gari.mp4'),
	(21, 15, 'Shadow Uchikomi', 'Shadow uchikomi o uchi gari.mp4'),
	(22, 16, 'Inti', 'Ippon seoi nage (video inti).mp4'),
	(23, 16, 'Shadow Uchikomi', 'Shadow uchikomi ippon seoi nage.mp4'),
	(24, 16, 'Posisi kaki', 'Posisi kaki ippon seoi nage.mp4'),
	(25, 16, 'Posisi Tangan', 'Posisi tangan Ippon seoi nage.mp4');
/*!40000 ALTER TABLE `program_videos` ENABLE KEYS */;

-- Dumping structure for table gorillaz.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `parent_name` varchar(200) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `school_name` varchar(200) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `profile_image` varchar(50) DEFAULT NULL,
  `about` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.users: ~15 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `birthday`, `parent_name`, `phone`, `gender`, `address`, `school_name`, `email`, `password`, `role`, `profile_image`, `about`) VALUES
	(1, 'nama baruy', '2020-07-08', '', '', 'L', 'Sekolah 1', 'Sekolah 1', 'email@dasd.copm', '$2y$10$X0Wr1tAFJZT9QcBaDGgf3e4uBGuMfp6sck99Ba2S0eVqxbJv39Ste', 'student', NULL, NULL),
	(2, 'nama2', '2020-07-08', '', '', 'P', 'Sekolah 2', 'Sekolah 2', 'nama@nama.com', '$2y$10$mhzPNiseelzkF2g2OHPoRejhZEpbxpbrwYSVdliiSR9wZvpuVt6S2', 'student', NULL, NULL),
	(3, 'adhi', '2020-07-08', 'a', '', 'L', 'Sekolah 3', 'Sekolah 3', 'rizkyadhicom@gmail.com', '$2y$10$GR70aU44XsKbLD6.ClVYDuWC9zOPKIkydbztPtFxzHA9VE4UI0t6m', 'student', 'Screenshot_3aa.png', NULL),
	(4, 'asdasdasd', '2020-07-08', '', '', 'L', 'Sekolah 4', 'Sekolah 4', 'asdasd@asdasd.com', '$2y$10$qEBjgD0cFc2sTSgcql8lie2gEi5TLm5W6MGoBM.JfhB9ClQbUbhYO', 'student', NULL, NULL),
	(5, 'instruktoraaaasdasd', '2020-07-08', '', '', 'L', 'Sekolah 5', '', 'i@i.com', '$2y$10$UqcctU8IFx.G8NpTTdqFsuvmTC7kN3Yk8DkEr/fVfLbAS9A9D3082', 'instructor', NULL, NULL),
	(6, 'nama', '2020-07-08', 'nama orang tua', '6285778425595', 'L', 'alamat', 'sekolah', 'admin@admin.com', '$2y$10$pLX.gzqw1tyxM8rdI4pWtuquD6cEjhqglS3.Y732XTAozYFn0H7Ui', 'admin', NULL, NULL),
	(7, 'asdasdasd', '2020-07-08', '', '3333', 'P', '1231231', '', 'asdas@ads.cp', '$2y$10$OfE6BL12BxxlMLxL799AXemrEZo85.f4V0hXMXWIcUdZG5P.sAZda', 'instructor', NULL, NULL),
	(8, 'Admin', '2020-07-25', 'asd', '12312', 'L', 'asd', 'asdas', 'admin1@admin.com', '$2y$10$GR70aU44XsKbLD6.ClVYDuWC9zOPKIkydbztPtFxzHA9VE4UI0t6m', 'admin', NULL, NULL),
	(9, 'nama lengkap[', '2020-07-08', '', '123123', 'L', 'alamat', '', 'email@asd.coma', '$2y$10$yg1WvtK91iGtND685p0mpuOfVMn5LcheINS8Kc.Ti/IixzhDRrupq', 'instructor', NULL, NULL),
	(10, 'rizky', '2020-07-19', 'adsfs', '1231231231231231', 'L', 'asdasd', 'sdfsdf', 'a@a.com', '$2y$10$NIuhU07tw7wRJLzSYK9I3./ccr4o77lK.OTK3S21D5UUNLnupRCVK', 'instructor', 'asd_2.jpg', NULL),
	(11, 'rizky', '2020-07-19', 'asdas', '1231231231231231', 'L', 'asdasd', 'asdasd', 'b@a.com', '', '', NULL, NULL),
	(12, 'rizky', '2020-07-19', 'asd', '1231231231231231', 'L', 'asdasd', 'asddd', 'c@a.com', '', '', NULL, NULL),
	(13, 'CACA', '1996-08-13', '', '0987654321', 'P', 'tajur', '', 'szalszamaulida@gmail.com', '$2y$10$P3RNwUfVtKPxtBIcHYc8aeTaumaIiuVkrXcWIbB8z6WSGY0nPeWpK', 'instructor', NULL, NULL),
	(14, 'R ADITYA NUR RIZKI', '2011-03-25', 'R SRI SUNARTI', '082258132622', 'L', 'bakom', 'SDN Tajur 1', 'kiki@gmail.com', '$2y$10$7Lfz/OUXa8sdR3/o6im37err6s9N/zsJiM7BEelqu0taOe7Xav9QG', 'student', 'kiki.JPG', NULL),
	(15, 'Pelatih', '1996-08-13', '', '0987654321', 'L', 'unitex', '', 'pelatih@gmail.com', '$2y$10$odTQ94mt.eKTXwgqjRC1jeKzTsWDm8axbr.FCf4GavytoPSF28tcK', 'instructor', NULL, NULL),
	(16, 'Dude Faizal Khadafi', '2011-03-13', 'Mohammad Firmansyah', '089687593718', 'L', 'Kp. Muara Tajur RT 01 Rw 02 kel. sindangrasa kec. bogor timur', 'SDN Tajur 1 Bogor', 'dude@gorillaz.atlet.com', '$2y$10$tUDj82dw3lyKFLJql1JTM.B1fiUlaoIHqX9cQ0lsS3jib1XUII0Ka', 'student', NULL, NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table gorillaz.user_achievements
DROP TABLE IF EXISTS `user_achievements`;
CREATE TABLE IF NOT EXISTS `user_achievements` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table gorillaz.user_achievements: ~3 rows (approximately)
DELETE FROM `user_achievements`;
/*!40000 ALTER TABLE `user_achievements` DISABLE KEYS */;
INSERT INTO `user_achievements` (`id`, `user_id`, `name`, `description`, `date`) VALUES
	(20, 1, 'asd', 'asd', '2020-07-24'),
	(21, 3, 'Penghargaan pertama', 'Pemenenang Kejuaraan as', '2020-07-23'),
	(22, 3, 'Kedua', 'aaa', '2020-07-15'),
	(23, 14, 'Juara 1 IJC karawang 2017', 'kejuaraan open SD SMP SMA', '2017-03-19');
/*!40000 ALTER TABLE `user_achievements` ENABLE KEYS */;

-- Dumping structure for table gorillaz.user_activities
DROP TABLE IF EXISTS `user_activities`;
CREATE TABLE IF NOT EXISTS `user_activities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `description` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `url` varchar(200) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- Dumping data for table gorillaz.user_activities: ~0 rows (approximately)
DELETE FROM `user_activities`;
/*!40000 ALTER TABLE `user_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_activities` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
